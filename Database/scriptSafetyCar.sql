drop database safety_car;
create database safety_car;

USE safety_car
create table car_brand
(
    id   int auto_increment
        primary key,
    name varchar(30) not null
);

create table car_colour
(
    id     int auto_increment
        primary key,
    colour varchar(20) not null
);

create table car_model
(
    id       int auto_increment
        primary key,
    name     varchar(50) not null,
    brand_id int         not null,
    constraint car_model_car_brand_id_fk
        foreign key (brand_id) references car_brand (id)
);

create table car
(
    id             int auto_increment
        primary key,
    model_id       int  not null,
    colour_id      int  not null,
    cubic_capacity int  not null,
    first_reg_date date not null,
    constraint car_car_brand_id_fk
        foreign key (model_id) references car_model (id),
    constraint car_car_colour_id_fk
        foreign key (colour_id) references car_colour (id)
);

create table criteria_range_table
(
    id          int auto_increment
        primary key,
    cc_min      int            not null,
    cc_max      int            not null,
    car_age_min int            not null,
    car_age_max int            not null,
    base_amount decimal(10, 2) not null
);

create table policy_status
(
    id     int auto_increment
        primary key,
    status varchar(20) null,
    constraint policy_status_status_uindex
        unique (status)
);

create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table users_details
(
    id                int auto_increment
        primary key,
    username          varchar(50)                          not null,
    first_name        varchar(20) charset utf8             not null,
    last_name         varchar(20) charset utf8             not null,
    email             varchar(65) charset utf8             not null,
    registered_date   datetime default current_timestamp() null,
    deactivation_date datetime                             null,
    last_edited_date  datetime default current_timestamp() null,
    constraint user_name
        unique (username),
    constraint `users_e-mail_uindex`
        unique (email),
    constraint users_details_fk
        foreign key (username) references users (username)
);

create table confirmation_token
(
    token_id           int auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_date       datetime     null,
    user_id            int          not null,
    constraint confirmationToken_users_details_id_fk
        foreign key (user_id) references users_details (id)
);

create table policies
(
    id                           int auto_increment
        primary key,
    user_id                      int                       not null,
    car_id                       int                       not null,
    driver_age                   int                       not null,
    accidents                    tinyint(1) default 0      not null,
    start_date                   date                      not null,
    email                        varchar(30)               not null,
    phone                        varchar(30) charset utf8  not null,
    address                      varchar(200) charset utf8 not null,
    status_id                    int        default 1      not null,
    base_amount                  decimal(10, 2)            not null,
    net_premium                  decimal(10, 2)            not null,
    total_premium                decimal(10, 2)            not null,
    car_registration_certificate varchar(255)              not null,
    submitted_date               date                      null,
    approved_date                date                      null,
    cancellation_date            date                      null,
    constraint policies_policy_status_id_fk
        foreign key (status_id) references policy_status (id),
    constraint policies_users_details_id_fk
        foreign key (user_id) references users_details (id)
);

