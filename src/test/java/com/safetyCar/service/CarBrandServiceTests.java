package com.safetyCar.service;

import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.repositories.CarRepository;
import com.safetyCar.services.CarService;
import com.safetyCar.services.CarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CarBrandServiceTests {

    @InjectMocks
    CarServiceImpl carService;

    @Mock
    CarRepository repository;

    @Test
    public void getById_Should_Succeed() {
        //Arrange
        int id = 1;
        carService.getById(id);
        //Act, Assert
        verify(repository, times(1)).findById(id);
    }

    @Test
    public void getAll_Should_Succeed() {
        when(repository.findAll()).thenReturn(new ArrayList<>());

        List<Brand> test = carService.getAllBrands();
        //Arrange,Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);
    }
}
