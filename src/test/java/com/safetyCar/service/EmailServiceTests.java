package com.safetyCar.service;

import com.safetyCar.services.EmailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.multipart.MultipartFile;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTests {

    @InjectMocks
    EmailServiceImpl emailService;

    @Mock
    JavaMailSender mailSender;

    @Test
    public void sendConfirmation_Should_Succeed() {
        //Arrange
        emailService.sendConfEmail("", "");
        // Assert
        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }

    @Test
    public void sendPolicyUpdate_Should_Succeed() {
        //Arrange
        emailService.sendPolicyUpdateEmail("", "", "");
        // Assert
        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}
