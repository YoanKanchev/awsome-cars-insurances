package com.safetyCar.service;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.InvalidRequestException;
import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.impl.Authority;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.models.impl.policy.PolicyStatus;
import com.safetyCar.repositories.PolicyRepository;
import com.safetyCar.repositories.PolicyStatusRepository;
import com.safetyCar.services.PolicyServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class PolicyServiceTests {

    @InjectMocks
    PolicyServiceImpl policyService;

    @Mock
    PolicyRepository policyRepository;

    @Mock
    PolicyStatusRepository statusRepository;

    @Test
    public void create_Should_Succeed() {
        //Arrange
        Policy testPolicy = new Policy();

        when(statusRepository.findByStatus("Pending")).thenReturn(new PolicyStatus(1, "Pending"));

        policyService.create(testPolicy);
        //Act, Assert
        verify(policyRepository, times(1)).create(testPolicy);
    }

    @Test
    public void getById_Should_Succeed() {
        //Arrange
        int id = 1;
        when(policyRepository.getById(id)).thenReturn(new Policy());

        policyService.getById(id);
        //Act, Assert
        verify(policyRepository, times(1)).getById(id);
    }

    @Test
    public void getAll_Should_Succeed() {
        when(policyRepository.getAll()).thenReturn(new ArrayList<>());

        List<Policy> test = policyService.getAll();
        //Arrange,Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);
    }

    @Test
    public void update_Should_Succeed() {
        //Arrange
        Policy policy = new Policy();
        UserDetails creator = new UserDetails();
        creator.setId(1);
        policy.setCreator(creator);

        when(policyRepository.update(policy)).thenReturn(policy);
        //Act
        Policy updatedPolicy = policyService.update(policy, creator);
        // Assert
        Assertions.assertEquals(policy, updatedPolicy);
    }

    @Test
    public void update_Should_Fail_When_Wrong_User() {
        //Arrange
        Policy policy = new Policy();
        UserDetails creator = new UserDetails();
        creator.setId(1);
        policy.setCreator(creator);

        UserDetails loggedUser = new UserDetails();
        loggedUser.setId(2);
        UserAuthentication loggedUserAuthentication = new UserAuthentication();
        Authority loggedUserAuthority = new Authority();
        loggedUserAuthority.setAuthority("ROLE_GUEST");
        loggedUserAuthentication.setAuthorities(new HashSet<>());
        loggedUserAuthentication.addAuthorities(loggedUserAuthority);
        loggedUser.setUserAuthentication(loggedUserAuthentication);

        //Act, Assert
        Assertions.assertThrows(AuthorisationException.class,
                () -> policyService.update(policy, loggedUser));
    }

    @Test
    public void validateTestPolicy_Should_Fail_When_Wrong_Input() {
        //Arrange
        CreatePolicyDto createPolicyDto = new CreatePolicyDto();
        createPolicyDto.setCarBrandId(1);
        createPolicyDto.setCarModelId(1);
        createPolicyDto.setRegistrationDate("date");
        createPolicyDto.setCubicCapacity(1);
        //Act, Assert
        Assertions.assertThrows(InvalidRequestException.class,
                () -> policyService.validateTestPolicy(createPolicyDto));
    }

    @Test
    public void filter_Should_Succeed() {
        when(policyRepository.filter(any(), anyInt(), any(), anyBoolean())).thenReturn(new ArrayList<>());

        List<Policy> test = policyService.filter(any(), anyInt(), any(), anyBoolean());
        //Arrange,Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);
    }

    @Test
    public void getStatusByName_Should_Succeed() {
        //Arrange
        String status = any();
        when(statusRepository.findByStatus(status)).thenReturn(new PolicyStatus());

        statusRepository.findByStatus(status);

        //Act, Assert
        verify(statusRepository, times(1)).findByStatus(status);
    }

    @Test
    public void getAllStatuses_Should_Succeed() {
        when(statusRepository.findAll()).thenReturn(new ArrayList<>());

        List<PolicyStatus> test = policyService.getAllStatuses();
        //Arrange,Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);
    }

    @Test
    public void changeStatus_Should_Succeed() {
        //Arrange
        int id = 1;
        String statusName = "";
        UserDetails user = new UserDetails();
        user.setId(1);

        Policy policyToUpdate = new Policy();
        policyToUpdate.setCreator(user);

        when(policyRepository.getById(id)).thenReturn(policyToUpdate);
        when(statusRepository.findByStatus(statusName)).thenReturn(new PolicyStatus());

        //Act
        policyService.changeStatus(id, statusName, user);

        //Act, Assert
        verify(policyRepository, times(1)).getById(id);
    }

}
