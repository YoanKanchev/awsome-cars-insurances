package com.safetyCar.service;

import com.safetyCar.exceptions.InvalidFileExtensionException;
import com.safetyCar.models.impl.policy.Premium;
import com.safetyCar.repositories.CriteriaTableRepository;
import com.safetyCar.services.CriteriaTableService;
import com.safetyCar.services.CriteriaTableServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;

@ExtendWith(MockitoExtension.class)
public class CriteriaTableServiceTests {
    @InjectMocks
    CriteriaTableServiceImpl criteriaTableService;

    @Mock
    CriteriaTableRepository repository;
    @Mock
    CriteriaTableService service;

    @Test
    public void getByCriteria_Should_Succeed() {
        //Arrange
        int cc = 1;
        int carAge = 1;
        when(repository.getByCriteria(cc, carAge)).thenReturn(new Premium());
        //Act
        criteriaTableService.getByCriteria(cc, carAge);
        // Assert
        verify(repository, times(1)).getByCriteria(cc, carAge);
    }

    @Test
    public void calculateTotalPremium_Should_ReturnCorrect() {
        //Arrange
        Premium basePremium = new Premium();
        basePremium.setBaseAmount(1);
        int driverAge = 18;
        //Act
        double totalPremium = criteriaTableService.calculateTotalPremium(basePremium, true, driverAge);
        //ASSERT
        Assertions.assertEquals(criteriaTableService.calculateTotalPremium(basePremium, true, driverAge), totalPremium);
    }

    @Test
    public void fileTypeValidation_Should_ThrowWhenEmpty() {
        //Arrange
        MockMultipartFile multipartFile = new MockMultipartFile("Name", new byte[0]);
        //Act,Assert
        Assertions.assertThrows(InvalidFileExtensionException.class,
                () -> criteriaTableService.fileTypeValidation(multipartFile));
    }

    @Test
    public void fileTypeValidation_Should_SucceedWhenNotEmpty() {
        //Arrange
        MockMultipartFile multipartFile = new MockMultipartFile("Name", new byte[0]);

        service.updateTable(multipartFile);
        //Act,Assert
        verify(service, times(1)).updateTable(multipartFile);
    }

    @Test
    public void uploadFile_Should_FailWhenEmptyFile() throws IOException {

//        Premium test = new Premium();
//        test.setId(1);
//        test.setCcMin(0);
//        test.setCcMax(1047);
//        test.setCarAgeMin(0);
//        test.setCcMax(19);
//        test.setBaseAmount(363.95);


        MockMultipartFile multipartFile = new MockMultipartFile(
                "criteria",
                "criteria.csv",
                MediaType.TEXT_PLAIN_VALUE,
                "id,cc_min,cc_max,car_age_min,car_age_max,base_amount".getBytes());

        Assertions.assertThrows(NullPointerException.class,
                () -> criteriaTableService.updateTable(multipartFile));
    }

//    @Test
//    public void updateTable_Should_Succeed() {
//
//        MultipartFile multipartFile = any(MultipartFile.class);
//
//        criteriaTableService.updateTable(multipartFile);
//        // Assert
//        verify(repository, times(1)).update(any(Premium.class));
//    }
}
