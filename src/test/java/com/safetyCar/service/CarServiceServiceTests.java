package com.safetyCar.service;

import com.safetyCar.models.impl.car.Car;
import com.safetyCar.models.impl.car.Colour;
import com.safetyCar.repositories.BrandRepository;
import com.safetyCar.repositories.CarRepository;
import com.safetyCar.repositories.ColourRepository;
import com.safetyCar.repositories.ModelRepository;
import com.safetyCar.services.CarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CarServiceServiceTests {

    @InjectMocks
    CarServiceImpl carService;

    @Mock
    CarRepository carRepository;
    @Mock
    BrandRepository brandRepository;
    @Mock
    ModelRepository modelRepository;
    @Mock
    ColourRepository colourRepository;

    @Test
    public void create_Should_Succeed() {
        //Arrange
        Car testCar = new Car();

        carService.create(testCar);
        //Act, Assert
        verify(carRepository, times(1)).save(testCar);
    }

    @Test
    public void update_Should_Succeed() {
        //Arrange
        Car testCar = new Car();

        carService.update(testCar);
        //Act, Assert
        verify(carRepository, times(1)).save(testCar);
    }

    @Test
    public void delete_Should_Succeed() {
        //Arrange
        Car testCar = new Car();

        carService.delete(testCar);
        //Act, Assert
        verify(carRepository, times(1)).delete(testCar);
    }

    @Test
    public void getById_Should_Succeed() {
        //Arrange
        int id = 1;

        carService.getBrandById(id);
        //Act, Assert
        verify(brandRepository, times(1)).getOne(id);
    }

    @Test
    public void getColourById_Should_Succeed() {
        //Arrange
        int id = 1;

        carService.getColourById(id);
        //Act, Assert
        verify(colourRepository, times(1)).getOne(id);
    }

    @Test
    public void getCarModelById_Should_Succeed() {
        //Arrange
        int id = 1;

        carService.getCarModelById(id);
        //Act, Assert
        verify(modelRepository, times(1)).getOne(id);
    }

    @Test
    public void getAll_Should_Succeed() {
        when(colourRepository.findAll()).thenReturn(new ArrayList<>());

        List<Colour> test = carService.getAllColour();
        //Arrange,Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);
    }
}
