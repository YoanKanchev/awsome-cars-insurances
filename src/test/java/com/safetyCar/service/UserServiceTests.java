package com.safetyCar.service;

import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.repositories.UserRepository;
import com.safetyCar.services.UserService;
import com.safetyCar.services.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static com.safetyCar.helpers.Constants.USER_ROLE;
import static com.safetyCar.helpers.UserFactory.createMockAuthentication;
import static com.safetyCar.helpers.UserFactory.createMockUser;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    UserServiceImpl userService;
    @InjectMocks
    Mapper mapper;

    @Mock
    UserRepository userRepository;
    @Mock
    UserService service;

    @Test
    public void create_ShouldThrow_WhenUserExists() {
        //Arrange
        CreateUserDto testUser = createMockUser();
        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));

        when(userRepository.isUsernamePresent(testUser.getUsername())).thenReturn(java.util.Optional.of(newUser));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(newUser));
        verify(userRepository, times(0)).createUser(newUser);
    }

    @Test
    public void create_ShouldThrow_WhenEmailExists() {
        //Arrange
        CreateUserDto testUser = createMockUser();

        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));

        when(userRepository.isEmailPresent(testUser.getEmail())).thenReturn(java.util.Optional.of(newUser));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(newUser));
        verify(userRepository, times(0)).createUser(newUser);
    }

    @Test
    public void create_ShouldCreateNewUser_WhenArgumentsAreUnique() {
        //Arrange
        CreateUserDto testUser = createMockUser();

        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));

        userService.createUser(newUser);

        //Act, Assert
        verify(userRepository, times(1)).createUser(newUser);
    }

    @Test
    public void getById_ShouldThrow_WhenIdNotPresent() {
        //Arrange
        when(userRepository.getById(1)).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getById(1));
        verify(userRepository, times((1))).getById(1);
    }

    @Test
    public void getById_Should_Succeed() {
        //Arrange
        when(userRepository.getById(1)).thenReturn(new UserDetails());
        userService.getById(1);

        //Act, Assert
        verify(userRepository, times(1)).getById(1);
    }

    @Test
    public void getByUsername_ShouldThrow_WhenUsernameNotPresent() {
        //Arrange
        when(userRepository.getByUsername("admin")).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername("admin"));
        verify(userRepository, times((1))).getByUsername("admin");
    }

    @Test
    public void getByUsername_Should_Succeed() {
        //Arrange
        CreateUserDto testUser = createMockUser();

        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));

        userService.createUser(newUser);

        when(userRepository.getByUsername("Spedicey")).thenReturn(newUser);

        userService.getByUsername("Spedicey");

        //Act, Assert
        verify(userRepository, times(1)).getByUsername("Spedicey");
    }

    @Test
    public void getAllUsers_Should_Succeed() {
        //Arrange
        when(userRepository.getAllUsers()).thenReturn(new ArrayList<>());

        List<UserDetails> allUsers = userService.getAllUsers();

        //Act, Assert
        Assertions.assertEquals(new ArrayList<>(), allUsers);
        verify(userRepository, times((1))).getAllUsers();
    }

    @Test
    public void getUserAuth_Should_Succeed() {
        //Arrange
        UserAuthentication test = new UserAuthentication();
        when(userRepository.getUserAuth("admin")).thenReturn(test);

        UserAuthentication userAuthentication = userService.getUserAuth("admin");

        //Act, Assert
        Assertions.assertEquals(test, userAuthentication);
        verify(userRepository, times((1))).getUserAuth("admin");
    }

//    @Test
//    public void updateUser_Should_UpdateUser() {
//        //Arrange
//        CreateUserDto changedUser = createMockUser();
//        UserDetails currentUser = new UserDetails();
//        currentUser.setUserAuthentication(createMockAuthentication("ROLE_USER", "admin", "random"));
//        currentUser.setFirstName("Tester");
//        currentUser.setLastName("Testov");
//        currentUser.setEmail("something@mail.com");
//        when(userService.encode(changedUser.getPassword())).thenReturn("$2a$12$3rsYpb7tCjucKz/0AVY.0u2UmlvIwy4sWsBIGYAL./YNs/Na5noAq");
//
//        //Act
//        UserDetails userToUpdate = userService.applyUserChanges(changedUser, currentUser);
//        userService.updateUser(userToUpdate,use);
//        //Assert
//        Assertions.assertEquals(currentUser, userToUpdate);
//    }

//    @Test
//    public void deleteUser_Should_Succeed() {
//        //Arrange
//        UserDetails testUser = new UserDetails();
//        testUser.setUserAuthentication(createMockAuthentication("ROLE_AGENT", "admin", "random"));
//
//        //Act
//        userService.updateAuthentication(testUser, USER_ROLE, false, true, true, true);
//        userService.deleteUser(testUser);
//
//        //Assert
//        verify(userRepository, times((1))).deleteUser(testUser);
//    }

    @Test
    public void filter_Should_Succeed() {
        //Arrange
        when(userRepository.filter(any(), any())).thenReturn(new ArrayList<>());

        List<UserDetails> test = userService.filter(any(), any());

        //Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);

        verify(userRepository, times((1))).filter(any(), any());
    }

    @Test
    public void getPoliciesByUser_Should_Succeed() {
        //Arrange
        when(userRepository.getPolicyByUser(1)).thenReturn(new ArrayList<>());

        List<Policy> test = userService.getPolicyByUserId(1);
        //Act, Assert
        Assertions.assertEquals(new ArrayList<>(), test);

        verify(userRepository, times((1))).getPolicyByUser(1);
    }

    @Test
    public void encodePassword_Should_Succeed() {
        //Arrange
        when(userService.encode("random")).thenReturn("$2a$12$3rsYpb7tCjucKz/0AVY.0u2UmlvIwy4sWsBIGYAL./YNs/Na5noAq");

        //Act, Assert
        Assertions.assertEquals(userService.encode("random"), "$2a$12$3rsYpb7tCjucKz/0AVY.0u2UmlvIwy4sWsBIGYAL./YNs/Na5noAq");
    }

//    @Test
//    public void createNewAuthentication_ShouldThrow_WhenUserExist() {
//        //Arrange
//        CreateUserDto testUser = createMockUser();
//        new UserDetails();
//        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));
//
//        when(userRepository.isUsernamePresent(testUser.getUsername())).thenReturn(java.util.Optional.of(newUser));
//
//        //Act, Assert
//        Assertions.assertThrows(DuplicateEntityException.class,
//                () -> userService.createAuthentication
//                        (testUser, "ROLE_USER", true, true, true, true));
//    }
//
//    @Test
//    public void createNewAuthentication_ShouldThrow_WhenEmailExist() {
//        //Arrange
//        CreateUserDto testUser = createMockUser();
//
//        UserDetails newUser = mapper.toUserDetails(testUser, createMockAuthentication("ROLE_USER", testUser.getUsername(), testUser.getPassword()));
//
//        Mockito.when(userRepository.isEmailPresent(testUser.getEmail())).thenReturn(java.util.Optional.of(newUser));
//
//        //Act, Assert
//        Assertions.assertThrows(DuplicateEntityException.class,
//                () -> userService.createAuthentication
//                        (testUser, "ROLE_USER", true, true, true, true));
//        Mockito.verify(service, Mockito.times((0))).createAuthentication(testUser, "ROLE_USER", true, true, true, true);
//    }
//
//    @Test
//    public void createNewAuthentication_ShouldSucceed_WhenValidArguments() {
//        //Arrange
//        CreateUserDto testUser = createMockUser();
//
//        //Act, Assert
//        Mockito.when(userService.encode("random")).thenReturn("$2a$12$3rsYpb7tCjucKz/0AVY.0u2UmlvIwy4sWsBIGYAL./YNs/Na5noAq");
//        userService.createAuthentication(testUser, USER_ROLE, false, true, true, true);
//        Mockito.verify(service, Mockito.times((0))).createAuthentication(testUser, USER_ROLE, false, true, true, true);
//    }
}
