package com.safetyCar.helpers;

import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.impl.Authority;
import com.safetyCar.models.impl.UserAuthentication;

import java.util.HashSet;
import java.util.Set;

public class UserFactory {
    public static CreateUserDto createMockUser() {
        return new CreateUserDto("Spedicey", "FirstName", "LastName", "random", "test@yahoo.bg");
    }
    public static UserAuthentication createMockAuthentication(String role, String username, String password) {
        UserAuthentication userAuthentication = new UserAuthentication();
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();

        userAuthentication.setUsername(username);
        userAuthentication.setPassword(password);
        authority.setAuthority(role);
        authority.setUserAuthentication(userAuthentication);
        authorities.add(authority);
        userAuthentication.setAuthorities(authorities);

        return userAuthentication;
    }

}
