function validateUsername() {
    const username = jQuery("#username").val();
    const url = "/api/users/searchBy/";

    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        data: {username: username},
        success: function (data) {
            if (data) {
                const html = "<p id='usernameWarning' style='color: darkred'>" + username + " is already taken!</p>";
                $("#usernameWarning").remove();

                $(html).hide().appendTo("#usernameText").fadeIn(1000);
            } else {
                $("#usernameWarning").fadeOut("medium", function () {
                    $(this).remove();
                });
            }
        }
    });
}

function validateEmail() {
    const emailReg = jQuery("#emailReg").val();
    const url = "http://localhost:8080/api/users/searchByEmail/";
    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        data: {email: emailReg},
        success: function (data) {
            if (data) {
                const html = "<p id='emailWarning' style='color: darkred'>" + emailReg + " is already taken!</p>";
                $("#emailWarning").remove();

                $(html).hide().appendTo("#emailText").fadeIn(1000);
            } else {
                $("#emailWarning").fadeOut("medium", function () {
                    $(this).remove();
                });
            }
        }
    });
}