$('input[type="submit"]').mousedown(function () {
    $(this).css('background', '#2ecc71');
});
$('input[type="submit"]').mouseup(function () {
    $(this).css('background', '#1abc9c');
});

$('#loginform').click(function () {
    $('.login').fadeToggle('slow');
    $(this).toggleClass('green');
});
$('#loginform2').click(function () {
    $('.login').fadeToggle('slow');
    $(this).toggleClass('green');
});

$('#signUpForm').click(function () {
    $('.signUp').fadeToggle('slow');
    $(this).toggleClass('green');
});


$(document).mouseup(function (e) {
    var container = $(".login");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
        $('#loginform').removeClass('green');
    }
});

$(document).mouseup(function (e) {
    const container = $(".signUp");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
        $('#signUpForm').removeClass('green');
    }
});

function modifyStateLogIn() {
    let stateObj = { id: "100" };
    window.history.replaceState(stateObj,
        "logIn", "/logIn");
}

function modifyStateSignUp() {
    let stateObj = { id: "100" };
    window.history.replaceState(stateObj,
        "signUp", "/signUp");
}