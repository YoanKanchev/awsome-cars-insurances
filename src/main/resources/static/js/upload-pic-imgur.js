$(document).ready(function () {
    $('#picture').change(function () {
        extensionChecker.call(this);
    });

    function extensionChecker() {
        const fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
            alert("Only formats are allowed : " + fileExtension.join(', '));
            debugger
            $(this).val('');
        } else {
            ajaxUploadPicture.call(this);
        }
    }

    function ajaxUploadPicture() {
        const reader = new FileReader();
        reader.onload = function (e) {
            //this next line separates url from data
            const iurl = e.target.result.substr(e.target.result.indexOf(",") + 1, e.target.result.length);
            const clientId = "028aa15b275f958";
            $.ajax({
                url: "https://api.imgur.com/3/image",
                type: "POST",
                datatype: "json",
                data: {
                    'image': iurl,
                    'type': 'base64'
                },
                success: uploadImageSuccess,//calling function which displays url
                error: function () {
                    alert("Uploading pictures is currently unavailable, sorry for the inconvenience!    :(")
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Client-ID " + clientId);
                }
            });
        };
        reader.readAsDataURL(this.files[0]);
    }
    function uploadImageSuccess(data) {
        let link = data.data.link;
        $("#pictureUrl").empty().append("<option value ='" + link + "'>Hello</option>");
    }
});