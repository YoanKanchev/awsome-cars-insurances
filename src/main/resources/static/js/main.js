$(function () {
    $("#form-total").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        autoFocus: true,
        transitionEffectSpeed: 500,
        titleTemplate: '<div class="title">#title#</div>',
        labels: {
            previous: 'Back',
            next: '<i class="zmdi zmdi-chevron-right"></i>',
            finish: '<i class="zmdi zmdi-chevron-right"></i>',
            current: ''
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            const modelName = '-- ' + $('#select_brand').val() + ' -- ' + $('#select_model').val();
            const email = $('#email').val();
            const phone = $('#phone').val();
            const regDate = $('#reg-date').val();
            const address = $('#address').val();
            const cc = $('#cubic-capacity').val();
            const age = $('#driver-age').val();
            const accidents = $('#driver-accident').val();
            const calc = $('#calc').text();
            const pic = $('#pictureUrl').val();

            $('#model-val').text(modelName);
            $('#email-val').text(email);
            $('#car-reg-val').text(regDate);
            $('#phone-val').text(phone);
            $('#address-val').text(address);
            $('#cc-val').text(cc);
            $('#age-val').text(age);
            $('#calculatedPremium-val').text(calc);
            $('#pic-val').text(pic);

            $('#accidents-val').text(accidents);

            return true;
        }
    });
});

$(function () {
    $("#select_brand").change(function () {

        const brandId = $(this).val();

        if (brandId !== "") {

            const url = "/api/brands/" + brandId + "/models";

            $.getJSON(url, {
                ajax: 'true'
            }, function (data) {
                const selectModel = $("#select_model");
                const len = data.length;
                let selectMessage;
                selectModel.empty();
                len === 0 ? selectMessage = "-- No models for this brand yet --" : selectMessage = "-- Select car model --";
                selectModel.append("<option value=''>" + selectMessage + "</option>");
                for (let i = 0; i < len; i++) {
                    const id = data[i]['id'];
                    const name = data[i]['name'];
                    selectModel.append("<option value='" + id + "'>" + name + "</option>");
                }
            })
        }else {
            const selectModel = $("#select_model");
            selectModel.empty();
            selectModel.append("<option value=''>" + "-- Select car model --" + "</option>");
        }
    })
});

document.getElementById("reg-date").max = new Date().toISOString().split("T")[0];

document.getElementById("star-date").min = new Date().toISOString().split("T")[0];


