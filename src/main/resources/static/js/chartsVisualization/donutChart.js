// Visualization API with the 'corechart' package.

$(document).ready(function () {
    google.charts.load('current', {'packages': ['corechart', 'controls']});
    google.charts.setOnLoadCallback(drawDonutChart);
});

function drawDonutChart(col) {
    const url = "http://localhost:8080/api/policies";

    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        success: function (data) {
            var sales = new google.visualization.DataTable();
            sales.addColumn('string', 'Date');
            sales.addColumn('string', 'Status');
            sales.addColumn('number', 'Base');
            sales.addColumn('number', 'Net');
            sales.addColumn('number', 'Gross');
            // Loop through each policy and populate the rows.
            $.each(data, function (index, value) {
                sales.addRow([
                    value.submittedDate.substring(0, 4),
                    value.status.status,
                    value.baseAmount,
                    value.netPremium,
                    value.totalPremium]);
            });
            sales.sort({column: 0});

            var dataByYear = google.visualization.data.group(
                sales,
                [{
                    column: 0,
                    type: 'string',
                    modifier: function (value) {
                        return value.toString();
                    }
                }, 1],
                [{
                    column: col,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }]
            );

            var dataAll = google.visualization.data.group(
                sales,
                [1],
                [{
                    column: col,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }]
            );

            var yearPicker = new google.visualization.ControlWrapper({
                controlType: 'CategoryFilter',
                containerId: 'categoryFilter_div',
                dataTable: dataByYear,
                options: {
                    filterColumnIndex: 0,
                    ui: {
                        allowTyping: false,
                        allowMultiple: false,
                        caption: 'All Years',
                        label: '',
                        labelStacking: 'vertical'
                    },
                    useFormattedValue: true
                }
            });

            google.visualization.events.addListener(yearPicker, 'statechange', function () {
                if (yearPicker.getState().selectedValues.length > 0) {
                    donutChart.setView({
                        columns: [1, 2],
                        rows: dataByYear.getFilteredRows([{
                            column: 0,
                            value: yearPicker.getState().selectedValues[0]
                        }])
                    });
                    donutChart.setDataTable(dataByYear);
                } else {
                    donutChart.setView(null);
                    donutChart.setDataTable(dataAll);
                }
                donutChart.draw();
            });
            yearPicker.draw();

            var donutChart = new google.visualization.ChartWrapper({
                chartType: 'PieChart',
                containerId: 'chart_div',
                dataTable: dataAll,
                options: {
                    title: 'Company Performance: Totals By Status, Filtered By Year',
                    height: 400,
                    pieHole: 0.4
                }
            });
            donutChart.draw();
        }
    });
}