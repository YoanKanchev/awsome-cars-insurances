$(document).ready(function () {
    google.charts.load('current', {'packages': ['corechart', 'controls']});
    google.charts.setOnLoadCallback(drawColumnChart);
});

function drawColumnChart() {
    const url = "http://localhost:8080/api/policies";

    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        success: function (data) {
            const sales = new google.visualization.DataTable();
            sales.addColumn('string', 'Date');
            sales.addColumn('string', 'Status');
            sales.addColumn('number', 'Base');
            sales.addColumn('number', 'Net');
            sales.addColumn('number', 'Gross');
            // Loop through each policy and populate the rows.
            $.each(data, function (index, value) {
                sales.addRow([
                    value.submittedDate.substring(0, 4),
                    value.status.status,
                    value.baseAmount,
                    value.netPremium,
                    value.totalPremium]);
            });
            sales.sort({column: 0});

            var dataByStatus = google.visualization.data.group(
                sales,
                [{
                    column: 0,
                    type: 'string',
                    modifier: function (value) {
                        return value.toString();
                    }
                }, 1],
                [{
                    column: 2,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 3,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 4,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }]
            );

            var dataAll = google.visualization.data.group(
                sales,
                [0],
                [{
                    column: 2,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 3,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 4,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }]
            );

            var statusPicker = new google.visualization.ControlWrapper({
                controlType: 'CategoryFilter',
                containerId: 'categoryFilter_div',
                dataTable: dataByStatus,
                options: {
                    filterColumnIndex: 1,
                    ui: {
                        allowTyping: false,
                        allowMultiple: false,
                        caption: 'All Statuses',
                        label: '',
                        labelStacking: 'vertical'
                    },
                    useFormattedValue: true
                }
            });

            google.visualization.events.addListener(statusPicker, 'statechange', function () {
                if (statusPicker.getState().selectedValues.length > 0) {
                    columnChart.setView({
                        columns: [0, 2, 3, 4],
                        rows: dataByStatus.getFilteredRows([{
                            column: 1,
                            value: statusPicker.getState().selectedValues[0]
                        }])
                    });
                    columnChart.setDataTable(dataByStatus);
                } else {
                    columnChart.setView(null);
                    columnChart.setDataTable(dataAll);
                }
                columnChart.draw();
            });
            statusPicker.draw();

            var columnChart = new google.visualization.ChartWrapper({
                chartType: 'ColumnChart',
                containerId: 'chart_div',
                dataTable: dataAll,
                options: {
                    title: 'Company Performance: Totals By Year, Filtered By Status',
                    hAxis: {title: 'Year'},
                    vAxis: {title: 'Amount'},
                    curveType: 'function',
                    legend: {position: 'bottom'},
                    height: 400
                }
            });
            columnChart.draw();
        }
    });
}