google.charts.load('current', {
    callback: drawDashboard,
    packages: ['controls']
});

function drawDashboard() {
    const url = "http://localhost:8080/api/policies";

    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        success: function (data) {

            var sales = new google.visualization.DataTable();
            sales.addColumn('string', 'Year');
            sales.addColumn('number', 'Base');
            sales.addColumn('number', 'Net');
            sales.addColumn('number', 'Gross');
            // Loop through each policy and populate the rows.
            $.each(data, function (index, value) {
                sales.addRow([
                    value.submittedDate.substring(0, 4),
                    value.baseAmount,
                    value.netPremium,
                    value.totalPremium]);
            });
            sales.sort({column: 0});

            var dataAll = google.visualization.data.group(
                sales,
                [0],
                [{
                    column: 1,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 2,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }, {
                    column: 3,
                    type: 'number',
                    aggregation: google.visualization.data.sum
                }]
            );

            var dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));

            var filter = new google.visualization.ControlWrapper({
                'controlType': 'CategoryFilter',
                'containerId': 'filter_div',
                'options': {
                    'filterColumnLabel': 'Year'
                },
                'ui': {
                    'allowTyping': false,
                    'allowMultiple': false,
                    'labelStacking': 'vertical'
                }
            });

            var Chart2 = new google.visualization.ChartWrapper({
                'chartType': 'LineChart',
                'containerId': 'chart2_div',
                'options': {
                    title: 'Totals Net vs Gross By Year',
                    hAxis: {title: 'Net Amount'},
                    vAxis: {title: 'Gross Amount'},
                    'width': 1000,
                    'height': 300,
                    curveType: 'function'
                },
                'view': {'columns': [2, 2, 3]}
            });

            google.visualization.events.addListener(Chart2, 'ready', function () {
                var Chart = new google.visualization.ChartWrapper({
                    'chartType': 'LineChart',
                    'containerId': 'chart1_div',
                    'options': {
                        title: 'Base Amount By Year',
                        hAxis: {title: 'Years'},
                        vAxis: {title: 'Base Amount'},
                        'width': 1000,
                        'height': 300
                    },
                    'dataTable': google.visualization.data.group(
                        Chart2.getDataTable(),
                        [0],
                        [{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number'}]
                    )
                });
                Chart.draw();
            });

            dashboard.bind(filter, Chart2);
            dashboard.draw(dataAll);
        }
    });
}