$(document).ready(() => {
    // GET REQUEST
    $("#btnCalculate").click((event) => {
        const selectBrand = $("#select_brand");
        const selectModel = $("#select_model");
        const regDate = $("#reg-date");
        const cC = $("#cubic-capacity");
        const driverAge = $("#driver-age");
        const hasAccidents = $("#driver-accident");

        if (selectBrand.val() === "" ||
            selectModel.val() === "" || regDate.val() === "" || cC.val() === "" || driverAge.val() === "") {
            checkIfHacked(selectBrand, selectModel, regDate, cC, driverAge);
        } else {
            event.preventDefault();
            ajaxGetTestPolicyCalculation(selectBrand.val(), selectModel.val(), regDate.val(), cC.val(), driverAge.val(), hasAccidents.val());
        }
    });

    // DO GET
    function ajaxGetTestPolicyCalculation(selectBrand, selectModel, regDate, cC, driverAge, hasAccidents) {
        $.ajax({
            url: "/api/policies/test",
            type: "POST",
            dataType: "json",
            data: {
                carBrandId: selectBrand,
                carModelId: selectModel,
                cubicCapacity: cC,
                registrationDate: regDate,
                driverAge: driverAge,
                hasAccidents: hasAccidents
            },
            success: getTestPremiumSuccess,
            error: function () {
                alert("Calculating policy is currently unavailable :(")
            }
        });
    }

    function getTestPremiumSuccess(data) {
        const formattedPremium = data.toFixed(2);
        $("#calc").remove();
        $("#reloadable").append("<p class='car-insurance-header' id='calc' >Your calculated premium is: " + formattedPremium + "lv.</p>");
        $("#loginToContinue").fadeIn(3000);
    }

    function checkIfHacked(selectBrand, selectModel, regDate, cC, driverAge) {
        if (!selectBrand.prop("required") || !selectModel.prop("required") || !regDate.prop("required") ||
            !cC.prop("required") || !driverAge.prop("required")) {
            alert("I see what you're trying to do, you little hacker you...not funny >:(")
        }
    }
});

