package com.safetyCar.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.Period;

public class CreatePolicyDto {

    @NotNull(message = "Car model cannot be empty!")
    @Min(value = 1, message = "You need to pick car model!")
    private Integer carModelId;

    @NotNull(message = "Car brand cannot be empty!")
    @Min(value = 1, message = "You need to pick car brand!")
    private Integer carBrandId;

    @NotNull(message = "Cubic capacity cannot be empty!")
    @Min(value = 1, message = "You need to specify cubic capacity")
    private Integer cubicCapacity;

    @NotNull(message = "Registration date cannot be empty!")
    @Size(min = 1, message = "Registration date cannot be empty!")
    private String registrationDate;

    @NotNull(message = "Driver age cannot be empty!")
    @Min(value = 18, message = "Driver cannot be under 18 years old!")
    private Integer driverAge;

    @NotNull(message = "Has accidents cannot be empty!")
    private boolean hasAccidents;

    @NotNull(message = "Start date cannot be empty!")
    @Size(min = 1, message = "Start date cannot be empty!")
    private String startDate;

    @NotNull(message = "Car colour cannot be empty!")
    private Integer carColourId;

    @NotNull(message = "E-mail cannot be empty!")
    @Email(message = "Incorrect e-mail")
    private String email;

    @NotNull(message = "Phone cannot be empty!")
    @Size(min = 10, message = "Incorrect phone number")
    private String phone;

    @NotNull(message = "Address cannot be empty!")
    @Size(min = 1, message = "Address cannot be empty!")
    private String address;

    @NotNull(message = "Incorrect picture")
    private String vehicleRegCertificatePic;

    private String carModelName;
    private Double premium;

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public void setCarModelId(Integer carModelId) {
        this.carModelId = carModelId;
    }

    public void setCarBrandId(Integer carBrandId) {
        this.carBrandId = carBrandId;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setDriverAge(Integer driverAge) {
        this.driverAge = driverAge;
    }

    public void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setCarColourId(Integer carColourId) {
        this.carColourId = carColourId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setVehicleRegCertificatePic(String vehicleRegCertificatePic) {
        this.vehicleRegCertificatePic = vehicleRegCertificatePic;
    }

    public Integer getCarModelId() {
        return carModelId;
    }

    public Integer getCarBrandId() {
        return carBrandId;
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public Integer getDriverAge() {
        return driverAge;
    }

    public boolean isHasAccidents() {
        return hasAccidents;
    }

    public String getStartDate() {
        return startDate;
    }

    public Integer getCarColourId() {
        return carColourId;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getVehicleRegCertificatePic() {
        return vehicleRegCertificatePic;
    }

    public Double getPremium() {
        return premium;
    }

    public String getFormattedPremium() {
        return String.format("%.2f", premium);
    }

    public Integer getCarAge() {
        if (registrationDate == null) {
            return -1;
        }
        LocalDate carRegDate = LocalDate.parse(registrationDate);
        Period period = Period.between(carRegDate, LocalDate.now());
        return period.getYears();
    }

    public String getCarModelName() {
        return carModelName;
    }
}
