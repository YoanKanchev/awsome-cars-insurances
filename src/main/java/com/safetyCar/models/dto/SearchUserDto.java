package com.safetyCar.models.dto;

public class SearchUserDto {

    public String username;
    public String role;

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
