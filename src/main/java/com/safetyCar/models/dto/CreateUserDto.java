package com.safetyCar.models.dto;

import com.safetyCar.models.impl.UserAuthentication;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

public class CreateUserDto {

    private int Id;
    @Size(min = 4, max = 30, message = "Username must be between 4 and 30 characters!")
    private String username;
    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;
    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;
    private String password;
    @Email(message = "Your email must contain @!")
    private String email;
    private UserAuthentication userAuthentication;
    private String registrationDate;
    private String deactivationDate;
    private String updateDate;
    private final LocalDateTime time;
    private boolean admin;

    public CreateUserDto(String username, String firstName, String lastName, String password, String email) {
        setUsername(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setEmail(email);
        time = LocalDateTime.now();
        setRegistrationDate(time);
        setUpdateDate(time);
        setDeactivationDate(null);
    }

    public CreateUserDto() {
        admin = false;
        time = LocalDateTime.now();
        setUpdateDate(time);
        setRegistrationDate(time);
    }

    public void setId(int id) {
        Id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserAuthentication getUserAuthentication() {
        return userAuthentication;
    }

    public void setUserAuthentication(UserAuthentication userAuthentication) {
        this.userAuthentication = userAuthentication;
    }

    public void setRegistrationDate(LocalDateTime time) {
        this.registrationDate = String.valueOf(time);
    }

    public void setDeactivationDate(String time) {
        this.deactivationDate = time;
    }

    public void setUpdateDate(LocalDateTime time) {
        this.updateDate = String.valueOf(time);
    }

    public int getId() {
        return Id;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public String getDeactivationDate() {
        return deactivationDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }
}