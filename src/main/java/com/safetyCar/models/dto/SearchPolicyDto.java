package com.safetyCar.models.dto;

public class SearchPolicyDto {

    public String username;
    public Integer statusID;
    public String sortBy;
    public boolean orderAsc;

    public String getUsername() {
        return username;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public String getSortBy() {
        return sortBy;
    }

    public boolean isOrderAsc() {
        return orderAsc;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public void setOrderAsc(boolean orderAsc) {
        this.orderAsc = orderAsc;
    }
}
