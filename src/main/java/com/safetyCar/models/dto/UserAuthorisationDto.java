package com.safetyCar.models.dto;

import java.util.ArrayList;
import java.util.List;

public class UserAuthorisationDto {
    private String username;
    private String password;
    private String email;
    private List<String> role;
    private boolean enabled;
    private boolean accountNonExpired;
    private boolean credentialsNonExpired;
    private boolean accountNonLocked;

    public UserAuthorisationDto() {
    }

    public UserAuthorisationDto(String username, String password, String email, List<String> roles, boolean enabled) {
        this(username, password, email, roles, enabled, true, true, true);
    }

    public UserAuthorisationDto(String username, String password, String email, List<String> role, boolean enabled,
                                boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked) {
        setUsername(username);
        setPassword(password);
        setEmail(email);
        setRole(role);
        setEnabled(enabled);
        setAccountNonExpired(accountNonExpired);
        setCredentialsNonExpired(credentialsNonExpired);
        setAccountNonLocked(accountNonLocked);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(List<String> role) {
        this.role = new ArrayList<>(role);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getRole() {
        return role;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public String getEmail() {
        return email;
    }
}
