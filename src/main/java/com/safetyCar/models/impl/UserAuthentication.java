package com.safetyCar.models.impl;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
public class UserAuthentication {
    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "userAuthentication", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Set<Authority> authorities;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getAuthoritiesList() {
        return authorities.stream()
                .map(Authority::getAuthority)
                .collect(Collectors.toList());
    }

    public Set<Authority> getAuthorities() {
        return new HashSet<>(authorities);
    }

    public void addAuthorities(Authority authority) {
        authorities.add(authority);
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }
}