package com.safetyCar.models.impl.policy;

import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.car.Car;

import javax.persistence.*;

@Entity
@Table(name = "policies")
public class Policy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetails creator;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "accidents")
    private boolean hasAccidents;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private PolicyStatus status;

    @Column(name = "base_amount")
    private double baseAmount;

    @Column(name = "net_premium")
    private double netPremium;

    @Column(name = "total_premium")
    private double totalPremium;

    @Lob
    @Column(name = "car_registration_certificate")
    private String vehicleRegCertificatePic;

    @Column(name = "submitted_date")
    private String submittedDate;

    @Column(name = "approved_date")
    private String approvedDate;

    @Column(name = "cancellation_date")
    private String cancellationDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreator(UserDetails creator) {
        this.creator = creator;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStatus(PolicyStatus status) {
        this.status = status;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public void setNetPremium(double netPremium) {
        this.netPremium = netPremium;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public void setVehicleRegCertificatePic(String vehicleRegCertificatePic) {
        this.vehicleRegCertificatePic = vehicleRegCertificatePic;
    }

    public void setSubmittedDate(String submittedDate) {
        this.submittedDate = submittedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public void setCancellationDate(String cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public UserDetails getCreator() {
        return creator;
    }

    public Car getCar() {
        return car;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public boolean isHasAccidents() {
        return hasAccidents;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public PolicyStatus getStatus() {
        return status;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public double getNetPremium() {
        return netPremium;
    }

    public double getTotalPremium() {
        return totalPremium;
    }

    public String getVehicleRegCertificatePic() {
        return vehicleRegCertificatePic;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public String getCancellationDate() {
        return cancellationDate;
    }
}
