package com.safetyCar.models.impl;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "confirmation_token")
public class ConfirmationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id")
    private int tokenId;

    @Column(name = "confirmation_token")
    private String confirmationToken;

    @Column(name = "created_date")
    private Date createDate;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private UserDetails user;

    public ConfirmationToken(UserDetails user, String confirmationToken) {
        this.user = user;
        setCreateDate();
        setConfirmationToken(confirmationToken);
//        confirmationToken = UUID.randomUUID().toString();
    }

    public ConfirmationToken() {
    }

    public int getTokenId() {
        return tokenId;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setTokenId(int tokenId) {
        this.tokenId = tokenId;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public void setCreateDate() {
        this.createDate = new Date();
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }
}
