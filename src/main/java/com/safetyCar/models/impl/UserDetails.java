package com.safetyCar.models.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name = "users_details")
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "registered_date")
    private String registrationDate;

    @Column(name = "deactivation_date")
    private String deactivationDate;

    @Column(name = "last_edited_date")
    private String updateDate;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "username")
    private UserAuthentication userAuthentication;

    public void setId(int id) {
        this.id = id;
    }

    public void setUserAuthentication(UserAuthentication userAuthorisation) {
        this.userAuthentication = userAuthorisation;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setDeactivationDate(String deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return userAuthentication.getUsername();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public String getDeactivationDate() {
        return deactivationDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public UserAuthentication getUserAuthentication() {
        return userAuthentication;
    }
}
