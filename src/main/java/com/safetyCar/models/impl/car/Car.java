package com.safetyCar.models.impl.car;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    @ManyToOne
    @JoinColumn(name = "colour_id")
    private Colour colour;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "first_reg_date")
    private String firstRegDate;


    public void setId(int id) {
        this.id = id;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public void setFirstRegDate(String firstRegDate) {
        this.firstRegDate = firstRegDate;
    }

    public int getId() {
        return id;
    }

    public Model getModel() {
        return model;
    }

    public Colour getColour() {
        return colour;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public String getFirstRegDate() {
        return firstRegDate;
    }

    public Integer getCarAge() {
        if (firstRegDate != null) {
            LocalDate carRegDate = LocalDate.parse(firstRegDate);
            Period period = Period.between(carRegDate, LocalDate.now());

            return period.getYears();
        }
        return null;
    }
}
