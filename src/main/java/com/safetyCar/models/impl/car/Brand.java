package com.safetyCar.models.impl.car;

import com.safetyCar.exceptions.EntityNotFoundException;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "car_brand")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "brand", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Set<Model> models;


    public void setModels(Set<Model> models) {
        new TreeSet<>(models);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Model> getModels() {
        return new ArrayList<>(models)
                .stream()
                .sorted(Comparator.comparing(Model::getName))
                .collect(Collectors.toList());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Model findModelById(int id) {
        return models.stream()
                .filter(model -> model.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(String.format("Model with id: %s doesn't exist", id)));
    }
}
