package com.safetyCar.services;

import com.safetyCar.models.impl.policy.Premium;
import org.springframework.web.multipart.MultipartFile;

public interface CriteriaTableService {
    Premium getByCriteria(Integer cubicCapacity, Integer carAge);

    double calculateNetPremium(Premium baseAmount, Boolean hasAccidents, Integer driverAge);

    double calculateTotalPremium(Premium netPremium, Boolean hasAccidents, Integer driverAge);

    void updateTable(MultipartFile file);

    void fileTypeValidation(MultipartFile file);
}
