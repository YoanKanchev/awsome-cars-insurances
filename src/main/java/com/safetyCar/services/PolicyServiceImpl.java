package com.safetyCar.services;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.InvalidRequestException;
import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.models.impl.policy.PolicyStatus;
import com.safetyCar.repositories.PolicyRepository;
import com.safetyCar.repositories.PolicyStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PolicyServiceImpl implements PolicyService {
    private final PolicyRepository policyRepository;
    private final PolicyStatusRepository policyStatusRepository;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository, PolicyStatusRepository policyStatusRepository) {
        this.policyRepository = policyRepository;
        this.policyStatusRepository = policyStatusRepository;
    }

    @Override
    public void create(Policy policy) {
        policy.setStatus(policyStatusRepository.findByStatus("Pending"));
        policy.setSubmittedDate(LocalDateTime.now().toString());
        policyRepository.create(policy);
    }

    @Override
    public Policy update(Policy policy, UserDetails user) {
        userAuthorisationValidation(policy, user);
        return policyRepository.update(policy);
    }

    @Override
    public Policy getById(int id) {
        return policyRepository.getById(id);
    }

    @Override
    public List<Policy> getAll() {
        return policyRepository.getAll();
    }

    @Override
    public List<Policy> filter(String user, Integer statusId, String sortBy, boolean isOrderAscending) {
        return policyRepository.filter(user, statusId, sortBy, isOrderAscending);
    }

    @Override
    public PolicyStatus getStatusByName(String status) {
        return policyStatusRepository.findByStatus(status);
    }

    @Override
    public void changeStatus(int policyId, String statusName, UserDetails user) {
        Policy policyToUpdate = getById(policyId);
        userAuthorisationValidation(policyToUpdate, user);
        PolicyStatus status = policyStatusRepository.findByStatus(statusName);
        policyToUpdate.setStatus(status);
        policyToUpdate.setApprovedDate(LocalDate.now().toString());
        update(policyToUpdate, user);
    }

    @Override
    public List<PolicyStatus> getAllStatuses() {
        return policyStatusRepository.findAll();
    }

    @Override
    public void validateTestPolicy(CreatePolicyDto policyDto) {
        if (policyDto == null
                || policyDto.getCarBrandId() == null
                || policyDto.getCarModelId() == null
                || policyDto.getRegistrationDate() == null
                || policyDto.getCubicCapacity() == null
                || policyDto.getDriverAge() == null
                || policyDto.getDriverAge() < 18
                || policyDto.getCubicCapacity() < 1) {
            throw new InvalidRequestException("All of fields must be submitted in order to calculate insurance!");
        }
    }

    private void userAuthorisationValidation(Policy policy, UserDetails loggedUser) {
        UserAuthentication userAuthentication = loggedUser.getUserAuthentication();
        if ((loggedUser.getId() != policy.getCreator().getId())
                && (userAuthentication.getAuthorities()
                .stream()
                .noneMatch(ua -> ua.getAuthority().equals("ROLE_ADMIN") || ua.getAuthority().equals("ROLE_AGENT")))) {
            throw new AuthorisationException(String.format("You have to be a creator of the policy or admin or agent to %s this policy!", "update"));
        }
    }
}
