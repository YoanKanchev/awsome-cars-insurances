package com.safetyCar.services;

import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.models.impl.car.Car;
import com.safetyCar.models.impl.car.Colour;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.repositories.BrandRepository;
import com.safetyCar.repositories.CarRepository;
import com.safetyCar.repositories.ColourRepository;
import com.safetyCar.repositories.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final ColourRepository colourRepository;
    private final BrandRepository brandRepository;
    private final ModelRepository modelRepository;


    @Autowired
    public CarServiceImpl(CarRepository carRepository, ColourRepository colourRepository,
                          BrandRepository brandRepository, ModelRepository modelRepository) {
        this.carRepository = carRepository;
        this.colourRepository = colourRepository;
        this.brandRepository = brandRepository;
        this.modelRepository = modelRepository;
    }

    @Override
    public Car create(Car car) {
        return carRepository.save(car);
    }

    @Override
    public void delete(Car car) {
        carRepository.delete(car);
    }

    @Override
    public Car update(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car getById(int id) {
        return carRepository.getOne(id);
    }

    @Override
    public Colour getColourById(int id) {
        return colourRepository.getOne(id);
    }

    @Override
    public Model getCarModelById(int id) {
        return modelRepository.getOne(id);
    }

    @Override
    public List<Colour> getAllColour() {
        return colourRepository.findAll();
    }

    @Override
    public Brand getBrandById(int id) {
        return brandRepository.getOne(id);
    }

    @Override
    public List<Brand> getAllBrands() {
        return brandRepository.findAll();
    }
}
