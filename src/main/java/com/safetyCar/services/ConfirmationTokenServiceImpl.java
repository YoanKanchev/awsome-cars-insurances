package com.safetyCar.services;

import com.safetyCar.models.impl.ConfirmationToken;
import com.safetyCar.repositories.ConfirmationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    private final ConfirmationTokenRepository tokenRepository;

    @Autowired
    public ConfirmationTokenServiceImpl(ConfirmationTokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public ConfirmationToken findByName(String name) {
        return tokenRepository.findByConfirmationToken(name);
    }

    @Override
    public void save(ConfirmationToken confirmationToken) {
        tokenRepository.save(confirmationToken);
    }
}
