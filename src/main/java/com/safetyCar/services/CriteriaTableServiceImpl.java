package com.safetyCar.services;


import com.safetyCar.exceptions.*;
import com.safetyCar.models.impl.policy.Premium;
import com.safetyCar.repositories.CriteriaTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Objects;

import static com.safetyCar.helpers.Constants.*;

@Service
public class CriteriaTableServiceImpl implements CriteriaTableService {

    private static final double ACCIDENT_COEFFICIENT = 1.20;
    private static final int EXPERIENCED_DRIVER_MIN_AGE = 25;
    private static final double DRIVER_AGE_COEFFICIENT = 1.05;
    private static final double TAX_COEFFICIENT = 1.10;

    private final CriteriaTableRepository criteriaTableRepository;

    @Autowired
    public CriteriaTableServiceImpl(CriteriaTableRepository criteriaTableRepository) {
        this.criteriaTableRepository = criteriaTableRepository;
    }

    @Override
    public Premium getByCriteria(Integer cubicCapacity, Integer carAge) {
        return criteriaTableRepository.getByCriteria(cubicCapacity, carAge);
    }

    @Override
    public double calculateNetPremium(Premium basePremium, Boolean hasAccidents, Integer driverAge) {
        double netPremium = basePremium.getBaseAmount();

        if (hasAccidents) netPremium *= ACCIDENT_COEFFICIENT;

        if (driverAge < EXPERIENCED_DRIVER_MIN_AGE) netPremium *= DRIVER_AGE_COEFFICIENT;

        return netPremium;
    }

    @Override
    public double calculateTotalPremium(Premium basePremium, Boolean hasAccidents, Integer driverAge) {
        return calculateNetPremium(basePremium, hasAccidents, driverAge) * TAX_COEFFICIENT;
    }

    @Override
    public void fileTypeValidation(MultipartFile file) {
        String fileName = Objects.requireNonNull(file.getOriginalFilename());

        if (file.isEmpty() || !fileName.contains(".csv")) {
            throw new InvalidFileExtensionException("Please select a CSV file to upload.");
        }
    }

    @Override
    public void updateTable(MultipartFile file) {
        int rows = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

            reader.readLine(); //skip headers row

            while (rows != 14) {
                rows++;

                String[] data = reader.readLine().split(",");

                if (data.length > 6) {
                    throw new InvalidFileExtraColumnException(String.format(FILE_EXTRA_CELL_FAIL, rows));
                } else if (data.length < 6) {
                    throw new InvalidFileMissingColumnException(String.format(FILE_MISSING_CELL_FAIL, rows));
                }

                emptyCellValidation(rows, data);

                int id = Integer.parseInt(data[0]);
                int ccMin = Integer.parseInt(data[1]);
                int ccMax = Integer.parseInt(data[2]);
                int carAgeMin = Integer.parseInt(data[3]);
                int carAgeMax = Integer.parseInt(data[4]);
                double baseAmount = Double.parseDouble(data[5]);

                Premium premiumRow = new Premium();
                premiumRow.setId(id);
                premiumRow.setCcMin(ccMin);
                premiumRow.setCcMax(ccMax);
                premiumRow.setCarAgeMin(carAgeMin);
                premiumRow.setCarAgeMax(carAgeMax);
                premiumRow.setBaseAmount(baseAmount);

                criteriaTableRepository.update(premiumRow);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void emptyCellValidation(int row, String[] data) {
        Arrays.stream(data).filter(String::isEmpty).forEach(cell -> {
            throw new InvalidFileEmptyCellException(String.format(FILE_EMPTY_CELL_FAIL, row));
        });
    }
}
