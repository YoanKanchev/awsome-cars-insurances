package com.safetyCar.services;

public interface EmailService {
    void sendConfEmail(String to, String token);

    void sendPolicyUpdateEmail(String to, String firstName, String status);
}
