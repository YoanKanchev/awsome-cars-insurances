package com.safetyCar.services;

import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.models.impl.car.Car;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.models.impl.car.Colour;

import java.util.List;

public interface CarService {

    Car create(Car car);

    void delete(Car car);

    Car update(Car car);

    Car getById(int id);

    List<Colour> getAllColour();

    Colour getColourById(int id);

    Model getCarModelById(int id);

    Brand getBrandById(int id);

    List<Brand> getAllBrands();

}
