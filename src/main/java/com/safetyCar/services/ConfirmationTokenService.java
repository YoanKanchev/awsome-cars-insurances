package com.safetyCar.services;

import com.safetyCar.models.impl.ConfirmationToken;

public interface ConfirmationTokenService {
    ConfirmationToken findByName(String name);

    void save(ConfirmationToken confirmationToken);
}
