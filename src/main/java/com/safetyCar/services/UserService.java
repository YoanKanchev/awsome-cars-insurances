package com.safetyCar.services;

import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;

import java.util.List;

public interface UserService {
    List<UserDetails> getAllUsers();

    UserDetails getById(int id);

    UserDetails getByUsername(String username);

    UserAuthentication getUserAuth(String username);

    void createUser(UserDetails user);

    void updateUser(CreateUserDto userDto, UserDetails userToUpdate);

    void deleteUser(UserDetails userToDelete, UserDetails loggedUser);

    void usernameDuplicationChecker(String username);

    void emailDuplicationChecker(String email);

    String encode(String pass);

    List<Policy> getPolicyByUserId(int id);

    void updateAuthentication(UserAuthorisationDto dto);

    void createAuthentication(UserAuthorisationDto dto);

    List<UserDetails> filter(String username, String role);
}