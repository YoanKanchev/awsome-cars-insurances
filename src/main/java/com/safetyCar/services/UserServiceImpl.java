package com.safetyCar.services;

import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository repository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return repository.getAllUsers();
    }

    @Override
    public UserDetails getById(int id) {
        return repository.getById(id);
    }

    @Override
    public UserDetails getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public UserAuthentication getUserAuth(String username) {
        return repository.getUserAuth(username);
    }

    @Transactional
    @Override
    public void createUser(UserDetails user) {
        repository.createUser(user);
    }

    @Override
    public void updateUser(CreateUserDto userDto, UserDetails userToUpdate) {
        userUpdater(userDto, userToUpdate);
        passwordUpdater(userDto, userToUpdate);

        repository.updateUser(userToUpdate);
    }

    @Override
    public void deleteUser(UserDetails userToDelete, UserDetails loggedUser) {
        userToDelete.setDeactivationDate(String.valueOf(LocalDate.now()));
        userToDelete.setUpdateDate(String.valueOf(LocalDate.now()));

        UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userToDelete.getUsername(),
                userToDelete.getUserAuthentication().getPassword(), userToDelete.getEmail(),
                userToDelete.getUserAuthentication().getAuthoritiesList(), false);
        updateAuthentication(authorisationDto);

        repository.updateUser(userToDelete);
    }

    @Override
    public List<Policy> getPolicyByUserId(int id) {
        return repository.getPolicyByUser(id);
    }

    @Override
    public void createAuthentication(UserAuthorisationDto dto) {
        usernameDuplicationChecker(dto.getUsername());
        emailDuplicationChecker(dto.getEmail());
        dto.setPassword(encode(dto.getPassword()));

        User user = toSpringSecurityUser(dto);
        userDetailsManager.createUser(user);
    }

    @Override
    public void updateAuthentication(UserAuthorisationDto dto) {
        User user = toSpringSecurityUser(dto);
        userDetailsManager.updateUser(user);
    }

    @Override
    public String encode(String pass) {
        return passwordEncoder.encode(pass);
    }

    @Override
    public List<UserDetails> filter(String username, String role) {
        return repository.filter(username, role);
    }

    @Override
    public void usernameDuplicationChecker(String username) {
        if (repository.isUsernamePresent(username).isPresent()) {
            throw new DuplicateEntityException("User already exists!");
        }
    }

    @Override
    public void emailDuplicationChecker(String email) {
        if (repository.isEmailPresent(email).isPresent()) {
            throw new DuplicateEntityException("Email already exists!");
        }
    }

    private void passwordUpdater(CreateUserDto userDto, UserDetails userToUpdate) {
        if (!userDto.getPassword().isEmpty()) {
            String password = encode(userDto.getPassword());
            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDto.getUsername(),
                    password, userDto.getEmail(), userToUpdate.getUserAuthentication().getAuthoritiesList(), true);
            updateAuthentication(authorisationDto);
        }
    }

    private void userUpdater(CreateUserDto userDto, UserDetails userToUpdate) {
        userToUpdate.setFirstName(userDto.getFirstName());
        userToUpdate.setLastName(userDto.getLastName());
        userToUpdate.setUpdateDate(userDto.getUpdateDate());
        if (!userDto.getEmail().equals(userToUpdate.getEmail())) emailDuplicationChecker(userDto.getEmail());
        userToUpdate.setEmail(userDto.getEmail());
    }

    private User toSpringSecurityUser(UserAuthorisationDto dto) {
        List<String> roles = dto.getRole();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roles.toArray(new String[0]));
        return new User(
                dto.getUsername(), dto.getPassword(), dto.isEnabled(), dto.isAccountNonExpired(),
                dto.isCredentialsNonExpired(), dto.isAccountNonLocked(), authorities);
    }
}
