package com.safetyCar.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static com.safetyCar.helpers.Constants.*;

@Service("emailSendService")
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendConfEmail(String to, String token) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(String.format(TO, to));
        mailMessage.setSubject(REG_SUBJECT);
        mailMessage.setFrom(FROM);
        mailMessage.setText(String.format(REG_TEXT, token));
        sendEmail(mailMessage);
    }

    @Override
    public void sendPolicyUpdateEmail(String to, String firstName, String status) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(String.format(TO, to));
        mailMessage.setSubject(POLICY_UPDATE_SUBJECT);
        mailMessage.setFrom(FROM);
        mailMessage.setText(String.format(POLICY_UPDATE_TEXT, firstName, status));
        sendEmail(mailMessage);
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        javaMailSender.send(email);
    }
}
