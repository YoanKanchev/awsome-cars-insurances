package com.safetyCar.helpers;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.services.EmailService;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

import static com.safetyCar.helpers.Constants.POLICY_STATUS_CHANGE_FORBIDDEN;
import static com.safetyCar.helpers.Constants.STATUS_CHANGE_SUCCESS;

@Component
public class PolicyStatusChanger {

    public String updatePolicyStatus(int policyId, String status, Principal principal, RedirectAttributes redirectAttributes, PolicyService policyService, UserService userService, EmailService emailService) {
        if (validatePolicyUpdatePossibility(policyId, redirectAttributes, policyService))
            return "redirect:/agent/manageInsurances";

        handlePolicyStatusUpdate(policyId, principal, status, policyService, userService);

        sendUpdateEmail(policyId, policyService, emailService);

        showMessage(redirectAttributes, "success", STATUS_CHANGE_SUCCESS);

        return "redirect:/agent/manageInsurances";
    }

    private boolean validatePolicyUpdatePossibility(int policyId, RedirectAttributes redirectAttributes, PolicyService policyService) {
        try {
            if (!isFutureStartDate(policyId, policyService)) {
                showMessage(redirectAttributes, "error", POLICY_STATUS_CHANGE_FORBIDDEN);
                return true;
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isFutureStartDate(int policyId, PolicyService policyService) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd");

        long currentDate = formatter
                .parse(LocalDateTime.now().toString())
                .getTime() / 1000;
        long policyStartDate = formatter
                .parse(policyService.getById(policyId).getStartDate())
                .getTime() / 1000;

        return currentDate < policyStartDate + 86400;
    }

    public static void handlePolicyStatusUpdate(int policyId, Principal principal, String status,
                                                PolicyService policyService, UserService userService) {
        try {
            UserDetails user = userService.getByUsername(principal.getName());
            policyService.changeStatus(policyId, status, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    private void sendUpdateEmail(int policyId, PolicyService policyService, EmailService emailService) {
        Policy policy = policyService.getById(policyId);

        emailService.sendPolicyUpdateEmail(
                policy.getEmail(),
                policy.getCreator().getFirstName(),
                policy.getStatus().getStatus());
    }

    private void showMessage(RedirectAttributes redirectAttributes, String model, String message) {
        redirectAttributes.addFlashAttribute(model, message);
    }
}
