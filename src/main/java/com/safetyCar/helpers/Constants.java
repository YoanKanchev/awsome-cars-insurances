package com.safetyCar.helpers;

public class Constants {

    //Roles
    public static final String USER_ROLE = "ROLE_USER";
    public static final String AGENT_ROLE = "ROLE_AGENT";
    public static final String ADMIN_ROLE = "ROLE_ADMIN";

    //Email
    public static final String TO = "%s";
    public static final String FROM = "tinqin.telerikacademy@gmail.com";

    //Email - user registration
    public static final String REG_SUBJECT = "Complete Registration";
    public static final String REG_TEXT = "To confirm your account, please click here : http://localhost:8080/verify?token=%s";

    //Email - policy status update
    public static final String POLICY_UPDATE_SUBJECT = "--Car insurance update--";
    public static final String POLICY_UPDATE_TEXT = "Dear %s,\nThere has been progress with your policy request. " +
            "Your policy has been moved to %s status.\n Kind Regards,\nSafety Car Tech Team";

    //Registration
    public static final String SUCC_REGISTRATION = "You have successfully completed your registration \n" +
            "A verification email has been send to you.\nPlease confirm your account before signing in!";
    public static final String EXPIRED_TOKEN = "Your token has expired!\nPlease request a new one.";
    public static final String SUCC_VERIFICATION = "Thank you for the confirmation.\nYour account is email verified. You may log in!";
    public static final String INVALID_TOKEN = "The token you are providing is invalid! Please request a new one.";
    public static final String SUCC_REG = "Account successfully created";

    //Profile
    public static final String SUCC_DELETED = "Your profile has been successfully deleted!";
    public static final String SUCC_CANCELLED = "Your request has been successfully withdrawn!";
    public static final String SUCC_CHANGED = "Your changes have been successfully saved!";
    public static final String USER_DELETE_SUCCESS = "User successfully deleted!";
    public static final String GIVE_RIGHTS_SUCCESS = "User has been successfully made an admin!";

    //Policy
    public static final String POLICY_CREATE_SUCCESS = "You have successfully posted insurance policy request.";
    public static final String POLICY_UPDATE_SUCCESS = "You have successfully updated policy request.";
    public static final String POLICY_STATUS_CHANGE_FORBIDDEN = "Changes pass policy start date are not allowed!";
    public static final String STATUS_CHANGE_SUCCESS = "Policy status has been successfully updated!";
    public static final String POLICY_NOT_FOUND = "Policy with id %d not found!";

    //Status
    public static final String CANCEL = "Cancelled";
    public static final String APPROVE = "Approved";
    public static final String REJECT = "Denied";
    public static final String PENDING = "Pending";

    //File
    public static final String FILE_UPDATE_SUCCESS = "Parameters updated successfully.";
    public static final String FILE_EXTRA_CELL_FAIL = "The uploaded file has more then 6 cells on row %d!";
    public static final String FILE_MISSING_CELL_FAIL = "The uploaded file has less then 6 cells on row %d!";
    public static final String FILE_EMPTY_CELL_FAIL = "The uploaded file contains an empty cell on row %d!";
    public static final String FILE_EMPTY_FAIL = "The uploaded file is empty!";



}
