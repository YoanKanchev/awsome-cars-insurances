package com.safetyCar.helpers;

import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Component
public class FilterHelper {

    public Predicate[] getPredicates(String user, Integer statusId,
                                     CriteriaBuilder criteriaBuilder, Root<Policy> root) {
        List<Predicate> predicates = new ArrayList<>();

        if (user != null && !user.isEmpty()) {
            predicates.add(criteriaBuilder.like(root.get("creator").get("userAuthentication").get("username"), "%"+user+"%"));
        }
        if (statusId != null && statusId != -1) {
            predicates.add(criteriaBuilder.equal(root.get("status"), statusId));
        }
        return predicates.toArray(new Predicate[0]);
    }

    public Predicate[] getUserPredicates(String user, String role, CriteriaBuilder criteriaBuilder, Root<UserDetails> root) {
        List<Predicate> users = new ArrayList<>();

        if (user != null && !user.equals("")) {
            users.add(criteriaBuilder
                    .equal(root.get("userAuthentication").get("username"), user));
        }
        if (role != null && !role.equals("")) {
            users.add(criteriaBuilder
                    .like(root.join("userAuthentication").join("authorities").get("authority"),
                            role));
        }

        return users.toArray(new Predicate[0]);
    }

    public void sortQuery(String sortBy, CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery,
                          Root<?> root, boolean isOrderAscending) {
        switch (sortBy) {
            case "id":
                sortAscendingOrDescending
                        (criteriaBuilder, criteriaQuery, root, isOrderAscending, "id");
                break;
            case "submittedDate":
                sortAscendingOrDescending
                        (criteriaBuilder, criteriaQuery, root, isOrderAscending, "submittedDate");
                break;
            case "totalPremium":
                sortAscendingOrDescending
                        (criteriaBuilder, criteriaQuery, root, isOrderAscending, "totalPremium");
                break;
            default:
                break;
        }
    }

    private void sortAscendingOrDescending(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery,
                                           Root<?> root, boolean isOrderAscending, String attr) {
        if (!isOrderAscending) {
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(attr)));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(attr)));
        }
    }
}
