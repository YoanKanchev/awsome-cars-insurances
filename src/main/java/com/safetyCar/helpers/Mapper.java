package com.safetyCar.helpers;

import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.models.impl.car.Car;
import com.safetyCar.models.impl.car.Colour;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.models.impl.policy.Premium;
import com.safetyCar.services.CarService;
import com.safetyCar.services.CriteriaTableService;
import com.safetyCar.services.PolicyService;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    public Car toCar(CreatePolicyDto policyDto,
                     CarService carService) {
        Car car = new Car();
        Colour carColour = carService.getColourById(policyDto.getCarColourId());
        Brand carBrand = carService.getBrandById(policyDto.getCarBrandId());
        Model model = carBrand.findModelById(policyDto.getCarModelId());
        car.setColour(carColour);
        car.setCubicCapacity(policyDto.getCubicCapacity());
        car.setFirstRegDate(policyDto.getRegistrationDate());
        car.setModel(model);
        return car;
    }

    public Policy toPolicy(CreatePolicyDto policyDto, UserDetails loggedUser,
                           CarService carService, CriteriaTableService criteriaTableService) {

        Policy policy = new Policy();
        Car car = toCar(policyDto, carService);
        carService.create(car);
        Premium premium = criteriaTableService.getByCriteria(policyDto.getCubicCapacity(), policyDto.getCarAge());

        double netPremium =
                criteriaTableService.calculateNetPremium(premium, policyDto.isHasAccidents(), policyDto.getDriverAge());
        double totalPremium =
                criteriaTableService.calculateTotalPremium(premium, policyDto.isHasAccidents(), policyDto.getDriverAge());

        policy.setCreator(loggedUser);
        policy.setCar(car);
        policy.setDriverAge(policyDto.getDriverAge());
        policy.setHasAccidents(policyDto.isHasAccidents());
        policy.setStartDate(policyDto.getStartDate());
        policy.setEmail(policyDto.getEmail());
        policy.setPhone(policyDto.getPhone());
        policy.setAddress(policyDto.getAddress());
        policy.setBaseAmount(premium.getBaseAmount());
        policy.setNetPremium(netPremium);
        policy.setTotalPremium(totalPremium);
        policy.setVehicleRegCertificatePic(policyDto.getVehicleRegCertificatePic());

        return policy;
    }

    public Policy policyUpdater(int id, CreatePolicyDto policyDto, PolicyService policyService) {

        Policy policy = policyService.getById(id);

        policy.setEmail(policyDto.getEmail());
        policy.setPhone(policyDto.getPhone());
        policy.setAddress(policyDto.getAddress());

        return policy;
    }

    public UserDetails toUserDetails(CreateUserDto userDto, UserAuthentication userAuthentication) {
        UserDetails user = new UserDetails();

        user.setUserAuthentication(userAuthentication);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setRegistrationDate(userDto.getRegistrationDate());
        user.setDeactivationDate(userDto.getDeactivationDate());
        user.setUpdateDate(userDto.getUpdateDate());

        return user;
    }
}
