package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.services.CarService;
import com.safetyCar.services.CriteriaTableService;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.stream.Collectors;

import static com.safetyCar.helpers.Constants.POLICY_CREATE_SUCCESS;
import static com.safetyCar.helpers.Constants.POLICY_UPDATE_SUCCESS;

@Controller
@SessionAttributes({"policyDtoSession", "error"})
@RequestMapping("/policies")
public class PolicyController {

    private final PolicyService policyService;
    private final CriteriaTableService criteriaTableService;
    private final CarService carService;
    private final UserService userService;
    private final Mapper mapper;

    @Autowired
    public PolicyController(PolicyService policyService, CriteriaTableService criteriaTableService,
                            CarService carService, UserService userService, Mapper mapper) {
        this.policyService = policyService;
        this.criteriaTableService = criteriaTableService;
        this.carService = carService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @PostMapping("/submit")
    public String submitInsurancePolicy(@Valid CreatePolicyDto policyDto, BindingResult bindingResult,
                                        RedirectAttributes redirectAttributes, Principal principal) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", bindingResult.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList()));

            Model model = carService.getCarModelById(policyDto.getCarModelId());
            policyDto.setCarModelName(model.getName());

            return "redirect:/";
        }
        try {
            UserDetails loggedUser = userService.getByUsername(principal.getName());
            Policy policyToCreate = mapper.toPolicy(policyDto, loggedUser, carService, criteriaTableService);
            policyService.create(policyToCreate);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        redirectAttributes.addFlashAttribute("success", POLICY_CREATE_SUCCESS);
        return "redirect:/";
    }

    @PostMapping("/{id}/update")
    public String update(@Valid @PathVariable int id, CreatePolicyDto policyDto,
                         BindingResult bindingResult, RedirectAttributes redirectAttributes,
                         Principal principal) {

        if (bindingResult.hasErrors()) {
            return "redirect:/profile/myInsurances";
        }

        try {
            Policy policyToUpdate = mapper.policyUpdater(id, policyDto, policyService);
            UserDetails loggedUser = userService.getByUsername(principal.getName());
            policyService.update(policyToUpdate, loggedUser);

        } catch (DuplicateEntityException dE) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, dE.getMessage());
        } catch (EntityNotFoundException eE) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, eE.getMessage());
        } catch (AuthorisationException unauthorised) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, unauthorised.getMessage());
        }
        redirectAttributes.addFlashAttribute("success", POLICY_UPDATE_SUCCESS);
        return "redirect:/profile/myInsurances";
    }

    @ModelAttribute
    private void addSessionDtoAttribute(org.springframework.ui.Model model, CreatePolicyDto policyDto) {
        model.addAttribute("policyDtoSession", policyDto);
    }
}
