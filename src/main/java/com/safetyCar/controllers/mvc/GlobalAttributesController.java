package com.safetyCar.controllers.mvc;

import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.SearchPolicyDto;
import com.safetyCar.models.dto.SearchUserDto;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class GlobalAttributesController {

    @ModelAttribute
    private void addNewUserDtoAttribute(Model model) {
        model.addAttribute("user", new CreateUserDto());
    }

    @ModelAttribute
    private void addSearchUserDtoAttribute(Model model) {
        model.addAttribute("searchUser", new SearchUserDto());
    }

    @ModelAttribute
    private void addSearchPolicyDtoAttribute(Model model) {
        model.addAttribute("search", new SearchPolicyDto());
    }

    @ModelAttribute
    private void addCreatePolicyDtoAttribute(Model model) {
        model.addAttribute("policyDto", new CreatePolicyDto());
    }
}
