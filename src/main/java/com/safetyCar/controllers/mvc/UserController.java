package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Objects;

import static com.safetyCar.helpers.Constants.*;

@Controller
@RequestMapping("/profile")
public class UserController {

    private final UserService userService;
    private final PolicyService policyService;

    @Autowired
    public UserController(UserService userService, PolicyService policyService) {
        this.userService = userService;
        this.policyService = policyService;
    }

    @GetMapping
    public String showProfilePage() {
        return "profilePage";
    }

    @GetMapping("/changes")
    public String showUserChangesPage() {
        return "profileChangesPage";
    }

    @PostMapping("/changes")
    public String handleUserChanges(@Valid CreateUserDto dto, BindingResult result,
                                    Principal principal, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            showMessage(redirectAttributes, "error", Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
            return "redirect:/profile/changes";
        }

        try {
            UserDetails userUpdate = userService.getByUsername(dto.getUsername());
            userService.updateUser(dto,userUpdate);

            showMessage(redirectAttributes,"success", SUCC_CHANGED);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            showMessage(redirectAttributes, "error", e.getMessage());
        }

        return "redirect:/profile/changes";
    }

    @PostMapping("/delete")
    public String deleteProfile(Principal principal, RedirectAttributes redirectAttributes) {
        try {
            UserDetails user = userService.getByUsername(principal.getName());
            userService.deleteUser(user,user);

            showMessage(redirectAttributes, "success", SUCC_DELETED);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return "redirect:/logout";
    }

    @GetMapping("/myInsurances")
    public String showMyInsurances(Model model, Principal principal) {
        try {
            UserDetails currentUser = userService.getByUsername(principal.getName());

            model.addAttribute("userPolicies", userService.getPolicyByUserId(currentUser.getId()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        return "myInsurances";
    }

    @PostMapping("/myInsurances/cancelPolicy/{policyId}")
    public String cancelPolicy(@PathVariable int policyId, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        try {
            UserDetails currentUser = userService.getByUsername(principal.getName());

            policyService.changeStatus(policyId, CANCEL, currentUser);

            model.addAttribute("userPolicies", userService.getPolicyByUserId(currentUser.getId()));

            showMessage(redirectAttributes, "success", SUCC_CANCELLED);
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "redirect:/profile/myInsurances";
    }

    private void showMessage(RedirectAttributes redirectAttributes, String model, String message) {
        redirectAttributes.addFlashAttribute(model, message);
    }

    @ModelAttribute
    private void addUserAttributes(Model model, Principal principal) {
        model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
    }
}
