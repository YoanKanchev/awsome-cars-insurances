package com.safetyCar.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/charts")
public class StatisticsController {

    @GetMapping("/chartNav")
    public String getNavigation() {
        return "chartNavigation";
    }

    @GetMapping("/columnChart")
    public String columnChart() {
        return "chartPages/columnChart";
    }

    @GetMapping("/lineChart")
    public String lineChart() {
        return "chartPages/lineChart";
    }

    @GetMapping("/pieChart")
    public String pieChart() {
        return "chartPages/pieChart";
    }

    @GetMapping("/donutChart")
    public String donutChart() {
        return "chartPages/donutChart";
    }

    @GetMapping("/complexLineChart")
    public String complexLineChart() {
        return "chartPages/complexLineChart";
    }
}
