package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.PolicyStatusChanger;
import com.safetyCar.models.dto.SearchPolicyDto;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.services.EmailService;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;

import static com.safetyCar.helpers.Constants.*;

@Controller
@RequestMapping("/agent")
public class AgentPortalController {

    private final PolicyService policyService;
    private final UserService userService;
    private final EmailService emailService;
    private final PolicyStatusChanger statusChange;

    @Autowired
    public AgentPortalController(PolicyService policyService, UserService userService, EmailService emailService, PolicyStatusChanger statusChange) {
        this.policyService = policyService;
        this.userService = userService;
        this.emailService = emailService;
        this.statusChange = statusChange;
    }

    @GetMapping
    public String showAgentPortalPage() {
        return "agentPortal";
    }

    @GetMapping("/manageInsurances")
    public String showAllInsurances(Model model) {
        model.addAttribute("userPolicies", policyService.getAll());
        model.addAttribute("statuses", policyService.getAllStatuses());

        return "manageInsurances";
    }

    @PostMapping("/search")
    public String filterInsurances(@ModelAttribute SearchPolicyDto dto, Model model) {
        try {
            List<Policy> userPolicies = policyService.filter(dto.getUsername(), dto.getStatusID(), dto.getSortBy(), dto.isOrderAsc());

            model.addAttribute("userPolicies", userPolicies);
            model.addAttribute("statuses", policyService.getAllStatuses());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return "manageInsurances";
    }

    @PostMapping("/cancelPolicy/{policyId}")
    public String cancelPolicyRequest(@PathVariable int policyId, Principal principal, RedirectAttributes redirectAttributes) {
        return statusChange.updatePolicyStatus(policyId, CANCEL, principal, redirectAttributes, policyService, userService, emailService);
    }

    @PostMapping("/rejectPolicy/{policyId}")
    public String rejectPolicyRequest(@PathVariable int policyId, Principal principal, RedirectAttributes redirectAttributes) {
        return statusChange.updatePolicyStatus(policyId, REJECT, principal, redirectAttributes, policyService, userService, emailService);
    }

    @PostMapping("/approvePolicy/{policyId}")
    public String approvePolicyRequest(@PathVariable int policyId, Principal principal, RedirectAttributes redirectAttributes) {
        return statusChange.updatePolicyStatus(policyId, APPROVE, principal, redirectAttributes, policyService, userService, emailService);
    }
}
