package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.*;
import com.safetyCar.services.CriteriaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import static com.safetyCar.helpers.Constants.FILE_UPDATE_SUCCESS;

@Controller
@RequestMapping
public class UploadController {
    private final CriteriaTableService criteriaTableService;

    @Autowired
    public UploadController(CriteriaTableService criteriaTableService) {
        this.criteriaTableService = criteriaTableService;
    }

    @PostMapping("/upload-csv-file")
    public String updateCriteriaTable(@RequestParam("file") MultipartFile file, Model model) {

        try {
            criteriaTableService.fileTypeValidation(file);
            criteriaTableService.updateTable(file);
            model.addAttribute("success", FILE_UPDATE_SUCCESS);
        } catch (InvalidFileExtensionException | InvalidFileExtraColumnException | InvalidFileMissingColumnException | InvalidFileEmptyCellException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "adminPortal";
    }
}
