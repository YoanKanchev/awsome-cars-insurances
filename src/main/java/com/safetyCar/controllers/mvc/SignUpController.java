package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.ConfirmationToken;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.repositories.ConfirmationTokenRepository;
import com.safetyCar.services.EmailService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static com.safetyCar.helpers.Constants.*;

@Controller
public class SignUpController {

    private final EmailService emailService;
    private final UserService userService;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final Mapper mapper;

    @Autowired
    public SignUpController(EmailService emailService,
                            UserService userService, Mapper mapper,
                            @Qualifier("confirmationTokenRepository") ConfirmationTokenRepository confirmationTokenRepository) {
        this.emailService = emailService;
        this.userService = userService;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.mapper = mapper;
    }

    @GetMapping("/signUp")
    public String showRegisterPage() {
        return "redirect:/";
    }

    @PostMapping("/signUp")
    public String handleSignUp(@Valid CreateUserDto userDto, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            showMessage(redirectAttributes, "error", Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
            return "redirect:/";
        }

        try {
            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDto.getUsername(),
                    userDto.getPassword(), userDto.getEmail(), Collections.singletonList(USER_ROLE), false);
            userService.createAuthentication(authorisationDto);

            UserDetails newUser = mapper.toUserDetails(userDto, userService.getUserAuth(userDto.getUsername()));
            userService.createUser(newUser);

            ConfirmationToken confirmationToken = confirmationTokenCreator(newUser);

            emailService.sendConfEmail(newUser.getEmail(), confirmationToken.getConfirmationToken());

        } catch (DuplicateEntityException e) {
            showMessage(redirectAttributes, "error", e.getMessage());
            return "redirect:/";
        }
        showMessage(redirectAttributes, "success", SUCC_REGISTRATION);
        return "redirect:/";
    }

    @GetMapping("/verify")
    public String confirmUserAccount(@RequestParam("token") String confirmationToken, RedirectAttributes redirectAttributes) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        return tokenValidityChecker(confirmationToken, redirectAttributes, token);
    }

    private String tokenValidityChecker(String confirmationToken, RedirectAttributes redirectAttributes, ConfirmationToken token) {
        if (token != null && token.getConfirmationToken().equals(confirmationToken)) {
            long currentDateSeconds = (new Date().getTime() / 1000);
            long tokenDateSeconds = (token.getCreateDate().getTime() / 1000);

            if (currentDateSeconds - tokenDateSeconds > 180) {
                showMessage(redirectAttributes, "error", EXPIRED_TOKEN);
                return "redirect:/";
            }
            activateUser(token);

            showMessage(redirectAttributes, "success", SUCC_VERIFICATION);
        } else {
            showMessage(redirectAttributes, "error", INVALID_TOKEN);
        }
        return "redirect:/";
    }

    private void activateUser(ConfirmationToken token) {
        UserDetails user = userService.getById(token.getUser().getId());
        UserAuthorisationDto authorisationDto = new UserAuthorisationDto(user.getUsername(),
                user.getUserAuthentication().getPassword(), user.getEmail(),
                Collections.singletonList(USER_ROLE), true);
        userService.updateAuthentication(authorisationDto);
    }

    private void showMessage(RedirectAttributes redirectAttributes, String model, String message) {
        redirectAttributes.addFlashAttribute(model, message);
    }

    private ConfirmationToken confirmationTokenCreator(UserDetails newUser) {
        ConfirmationToken confirmationToken = new ConfirmationToken(newUser, UUID.randomUUID().toString());
        confirmationTokenRepository.save(confirmationToken);
        return confirmationToken;
    }
}
