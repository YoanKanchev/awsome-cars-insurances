package com.safetyCar.controllers.mvc;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.helpers.PolicyStatusChanger;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.SearchUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.services.EmailService;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.safetyCar.helpers.Constants.*;

@Controller
@RequestMapping("/admin")
public class AdminPortalController {

    private final PolicyService policyService;
    private final UserService userService;
    private final Mapper mapper;
    private final PolicyStatusChanger statusChange;
    private final EmailService emailService;

    @Autowired
    public AdminPortalController(PolicyService policyService, UserService userService, Mapper mapper,
                                 PolicyStatusChanger statusChange, EmailService emailService) {
        this.policyService = policyService;
        this.userService = userService;
        this.mapper = mapper;
        this.statusChange = statusChange;
        this.emailService = emailService;
    }

    @GetMapping
    public String showAdminPortalPage() {
        return "adminPortal";
    }

    @GetMapping("/createUser")
    public String showCreateUser() {
        return "adminCreateUser";
    }

    @PostMapping("/createUser")
    public String submitNewUser(@Valid CreateUserDto userDto, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            showMessage(redirectAttributes, "error", Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
            return "redirect:/admin/createUser";
        }

        try {
            String role = userDto.isAdmin() ? ADMIN_ROLE : AGENT_ROLE;

            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDto.getUsername(),
                    userDto.getPassword(), userDto.getEmail(),
                    Collections.singletonList(role), true);
            userService.createAuthentication(authorisationDto);

            UserDetails newUser = mapper.toUserDetails(userDto, userService.getUserAuth(userDto.getUsername()));
            userService.createUser(newUser);

            showMessage(redirectAttributes, "success", SUCC_REG);

            return "redirect:/admin/createUser";
        } catch (DuplicateEntityException e) {
            showMessage(redirectAttributes, "error", e.getMessage());
            return "redirect:/admin/createUser";
        }
    }

    @GetMapping("/searchUser")
    public String showAllUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "adminAllUsers";
    }

    @PostMapping("/searchUser")
    public String searchForUsers(SearchUserDto dto, Model model) {
        try {
            List<UserDetails> users = userService.filter(dto.getUsername(), dto.getRole());

            model.addAttribute("users", users);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
        return "adminAllUsers";
    }

    @PostMapping("/giveRights/{userId}")
    public String giveRights(@PathVariable int userId, RedirectAttributes redirectAttributes) {
        try {
            UserDetails userDetails = userService.getById(userId);

            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDetails.getUsername(),
                    userDetails.getUserAuthentication().getPassword(), userDetails.getEmail(),
                    Collections.singletonList(ADMIN_ROLE), true);

            userService.updateAuthentication(authorisationDto);

            showMessage(redirectAttributes, "success", GIVE_RIGHTS_SUCCESS);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        return "redirect:/admin/searchUser";
    }

    @PostMapping("/delete/{userId}")
    public String deleteUser(@PathVariable int userId, RedirectAttributes redirectAttributes, Principal principal) {
        try {
            UserDetails userToDelete = userService.getById(userId);
            UserDetails loggedUser = userService.getByUsername(principal.getName());

            userService.deleteUser(userToDelete, loggedUser);

            showMessage(redirectAttributes, "success", USER_DELETE_SUCCESS);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        return "redirect:/admin/searchUser";
    }

    @PostMapping("/revertPolicy/{policyId}")
    public String revertPolicyRequest(@PathVariable int policyId, Principal principal, RedirectAttributes redirectAttributes) {
        return statusChange.updatePolicyStatus(policyId, PENDING, principal, redirectAttributes, policyService, userService, emailService);
    }

    private void showMessage(RedirectAttributes redirectAttributes, String model, String message) {
        redirectAttributes.addFlashAttribute(model, message);
    }
}
