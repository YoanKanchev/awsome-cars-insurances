package com.safetyCar.controllers.mvc;

import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.models.impl.car.Colour;
import com.safetyCar.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/")
public class HomeController {

    private final CarService carService;

    @Autowired
    public HomeController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public String showHomePage(
            @SessionAttribute(value = "policyDtoSession", required = false) CreatePolicyDto createPolicyDto, Model model) {
        List<Brand> brands = carService.getAllBrands();
        List<Colour> colours = carService.getAllColour();

        model.addAttribute("policyDto", Objects.requireNonNullElseGet(createPolicyDto, CreatePolicyDto::new));
        model.addAttribute("colours", colours);
        model.addAttribute("brands", brands);

        return "index";
    }

    @GetMapping("/logIn")
    public String showLogIn() {
        return "redirect:/";
    }

    @GetMapping("/failCredentials")
    public String failCredentials(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", "Wrong username or password!");
        return "redirect:/";
    }
}
