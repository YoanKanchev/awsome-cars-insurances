package com.safetyCar.controllers.error;


import com.safetyCar.models.dto.CreatePolicyDto;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
@SessionAttributes({"policyDtoSession", "error"})
public class MultipartExceptionHandler {

    @ExceptionHandler(MultipartException.class)
    public String handleError1(MultipartException e, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("error", e.getCause().getMessage());
        return "redirect:/";
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("error", e.getCause().getMessage());
        return "redirect:/";
    }

    @ModelAttribute
    private void addSessionDtoAttribute(Model model, CreatePolicyDto policyDto) {
        model.addAttribute("policyDtoSession", policyDto);
    }
}
