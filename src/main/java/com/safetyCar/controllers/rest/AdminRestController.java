package com.safetyCar.controllers.rest;

import com.safetyCar.exceptions.AuthorisationException;
import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;

import static com.safetyCar.helpers.Constants.*;
import static com.safetyCar.helpers.PolicyStatusChanger.handlePolicyStatusUpdate;

@RestController
@RequestMapping("/api/adminPortal")
public class AdminRestController {
    private final UserService service;
    private final PolicyService policyService;
    private final Mapper mapper;

    @Autowired
    public AdminRestController(UserService service, PolicyService policyService, Mapper mapper) {
        this.service = service;
        this.policyService = policyService;
        this.mapper = mapper;
    }

    @PostMapping("/createAgent")
    public UserDetails createAgent(@Valid @RequestBody CreateUserDto userDto) {
        try {
            return createTechnicalUser(AGENT_ROLE, userDto);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/createAdmin")
    public UserDetails createAdmin(@Valid @RequestBody CreateUserDto userDto) {
        try {
            return createTechnicalUser(ADMIN_ROLE, userDto);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/giveAdminRights/")
    public UserDetails giveAdminRights(@RequestParam int userId) {
        UserDetails userDetails = service.getById(userId);

        UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDetails.getUsername(),
                userDetails.getUserAuthentication().getPassword(), userDetails.getEmail(),
                Collections.singletonList(ADMIN_ROLE), true);

        service.updateAuthentication(authorisationDto);

        return userDetails;
    }

    @DeleteMapping("/deleteUser/")
    public UserDetails deleteUser(@RequestParam int userId, Principal principal) {
        try {
            UserDetails userToDelete = service.getById(userId);
            UserDetails loggedUser = service.getByUsername(principal.getName());

            service.deleteUser(userToDelete, loggedUser);

            return userToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/revertPolicy/")
    public String rejectPolicy(@RequestParam int policyId, Principal principal) {

        handlePolicyStatusUpdate(policyId, principal, APPROVE, policyService, service);

        return "Policy updated successfully.";

    }

    private UserDetails createTechnicalUser(String role, CreateUserDto userDto) {
        try {
            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDto.getUsername(),
                    userDto.getPassword(), userDto.getEmail(),
                    Collections.singletonList(role), false);

            service.createAuthentication(authorisationDto);

            UserDetails user = mapper.toUserDetails(userDto, service.getUserAuth(userDto.getUsername()));

            service.createUser(user);

            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorisationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }
}
