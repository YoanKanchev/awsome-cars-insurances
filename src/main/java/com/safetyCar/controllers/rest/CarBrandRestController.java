package com.safetyCar.controllers.rest;

import com.safetyCar.models.impl.car.Brand;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.services.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/brands")
public class CarBrandRestController {
    private final CarService carService;

    public CarBrandRestController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public List<Brand> getAll() {
        return carService.getAllBrands();
    }

    @GetMapping("/{id}")
    public Brand searchById(@PathVariable int id) {
        try {
            return carService.getBrandById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/models")
    public List<Model> getModelsByBrand(@PathVariable int id) {
        try {
            Brand brand = carService.getBrandById(id);
            return new ArrayList<>(brand.getModels());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
