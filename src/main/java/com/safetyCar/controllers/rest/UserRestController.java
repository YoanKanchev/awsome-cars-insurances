package com.safetyCar.controllers.rest;

import com.safetyCar.exceptions.DuplicateEntityException;
import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreateUserDto;
import com.safetyCar.models.dto.UserAuthorisationDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.repositories.UserRepository;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.safetyCar.helpers.Constants.*;
import static com.safetyCar.helpers.PolicyStatusChanger.handlePolicyStatusUpdate;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService service;
    private final PolicyService policyService;
    private final Mapper mapper;
    private final UserRepository userRepository;

    @Autowired
    public UserRestController(UserService service, PolicyService policyService, Mapper mapper, UserRepository userRepository) {
        this.service = service;
        this.policyService = policyService;
        this.mapper = mapper;
        this.userRepository = userRepository;
    }

    @GetMapping()
    public List<UserDetails> getAllUsers() {
        return service.getAllUsers();
    }

    @GetMapping("/searchBy/")
    public Object searchBy
            (@Valid @RequestParam(required = false) String username,
             @Valid @RequestParam(required = false) Integer id) {
        try {
            if (username != null) {
                return userRepository.isUsernamePresent(username);
            } else if (id != null && id > 0) {
                return service.getById(id);
            } else {
                return new ArrayList<UserDetails>();
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/searchByEmail/")
    public Object searchByEmail
            (@Valid @RequestParam(required = false) String email) {
        try {
            if (email != null) {
                return userRepository.isEmailPresent(email);
            } else {
                return new ArrayList<UserDetails>();
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/myPolicies")
    public List<Policy> getMyPolicies(Principal principal) {
        try {
            UserDetails currentUser = service.getByUsername(principal.getName());

            return service.getPolicyByUserId(currentUser.getId());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/manageInsurances/cancel/")
    public String cancelMyPolicy(@RequestParam int policyId, Principal principal) {

        handlePolicyStatusUpdate(policyId, principal, CANCEL, policyService, service);

        return "Policy updated successfully.";

    }

    @PutMapping("/manageInsurances/approve/")
    public String approvePolicy(@RequestParam int policyId, Principal principal) {

        handlePolicyStatusUpdate(policyId, principal, APPROVE, policyService, service);

        return "Policy updated successfully.";

    }

    @PutMapping("/manageInsurances/reject/")
    public String rejectPolicy(@RequestParam int policyId, Principal principal) {

        handlePolicyStatusUpdate(policyId, principal, REJECT, policyService, service);

        return "Policy updated successfully.";

    }

    @PostMapping("/createUser")
    public UserDetails createUser(@Valid @RequestBody CreateUserDto userDto) {
        try {
            UserAuthorisationDto authorisationDto = new UserAuthorisationDto(userDto.getUsername(),
                    userDto.getPassword(), userDto.getEmail(),
                    Collections.singletonList(USER_ROLE), false);

            service.createAuthentication(authorisationDto);

            UserDetails user = mapper.toUserDetails(userDto, service.getUserAuth(userDto.getUsername()));

            service.createUser(user);

            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/updateUser")
    public UserDetails updateUser(@Valid @RequestBody CreateUserDto userDto, Principal principal) {
        try {
            UserDetails userUpdate = service.getByUsername(userDto.getUsername());

            service.updateUser(userDto, userUpdate);

            return userUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/")
    public String selfDelete(Principal principal) {
        try {
            UserDetails user = service.getByUsername(principal.getName());
            service.deleteUser(user, user);

            return String.format("User %s successfully deleted!", user.getUsername());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}