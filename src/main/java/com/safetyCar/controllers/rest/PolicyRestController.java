package com.safetyCar.controllers.rest;

import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.exceptions.InvalidRequestException;
import com.safetyCar.helpers.Mapper;
import com.safetyCar.models.dto.CreatePolicyDto;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.car.Model;
import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.models.impl.policy.Premium;
import com.safetyCar.services.CarService;
import com.safetyCar.services.CriteriaTableService;
import com.safetyCar.services.PolicyService;
import com.safetyCar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@SessionAttributes({"policyDtoSession", "error"})
@RequestMapping("/api/policies")
public class PolicyRestController {
    private final PolicyService policyService;

    private final CarService carService;
    private final CriteriaTableService criteriaTableService;
    private final UserService userService;
    private final Mapper mapper;


    @Autowired
    public PolicyRestController(PolicyService policyService, CarService carService,
                                CriteriaTableService criteriaTableService, UserService userService, Mapper mapper) {
        this.policyService = policyService;
        this.carService = carService;
        this.criteriaTableService = criteriaTableService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping()
    public List<Policy> getAll() {
        return policyService.getAll();
    }

    @GetMapping("/{id}")
    public Policy getById(@PathVariable int id) {
        try {
            return policyService.getById(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/test")
    public Double calculateTestPolicy(@ModelAttribute CreatePolicyDto policyDto, org.springframework.ui.Model model) {
        try {
            policyService.validateTestPolicy(policyDto);
            Model carModel = carService.getCarModelById(policyDto.getCarModelId());
            Premium premium = criteriaTableService.getByCriteria(policyDto.getCubicCapacity(), policyDto.getCarAge());
            double totalPremium =
                    criteriaTableService.calculateTotalPremium(premium, policyDto.isHasAccidents(), policyDto.getDriverAge());

            policyDto.setPremium(totalPremium);
            policyDto.setCarModelName(carModel.getName());
            model.addAttribute("policyDtoSession", policyDto);

            return totalPremium;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidRequestException iR) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, iR.getMessage());
        }
    }

    @PostMapping("/create")
    public Policy createPolicy(@Valid @RequestBody CreatePolicyDto policyDto, Principal principal) {
        try {

            UserDetails loggedUser = userService.getByUsername(principal.getName());
            Policy policyToCreate = mapper.toPolicy(policyDto, loggedUser, carService, criteriaTableService);
            policyService.create(policyToCreate);

            return policyToCreate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/")
    public List<Policy> filter(@RequestParam(required = false) String username,
                               @RequestParam(required = false) Integer statusId,
                               @RequestParam(required = false) String sortBy,
                               @RequestParam(required = false) boolean isOrderAscending) {

        return policyService.filter(username, statusId, sortBy, isOrderAscending);
    }
}
