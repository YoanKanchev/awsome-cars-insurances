package com.safetyCar.exceptions;

public class InvalidFileEmptyCellException extends RuntimeException {
    public InvalidFileEmptyCellException (String message) {super(message); }
}
