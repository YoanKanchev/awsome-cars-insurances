package com.safetyCar.exceptions;

public class InvalidFileExtraColumnException extends RuntimeException {
    public InvalidFileExtraColumnException (String message) {super(message); }
}
