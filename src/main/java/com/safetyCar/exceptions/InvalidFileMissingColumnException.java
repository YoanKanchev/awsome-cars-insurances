package com.safetyCar.exceptions;

public class InvalidFileMissingColumnException extends RuntimeException {
    public InvalidFileMissingColumnException (String message) {super(message); }
}
