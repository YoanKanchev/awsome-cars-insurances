package com.safetyCar.exceptions;

public class InvalidFileExtensionException extends RuntimeException {
    public InvalidFileExtensionException (String message) {
        super(message);
    }

}
