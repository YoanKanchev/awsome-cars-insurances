package com.safetyCar.repositories;

import com.safetyCar.exceptions.EntityNotFoundException;
import com.safetyCar.helpers.FilterHelper;
import com.safetyCar.models.impl.policy.Policy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

import static com.safetyCar.helpers.Constants.POLICY_NOT_FOUND;


@Repository
public class PolicyRepositoryImpl implements PolicyRepository {
    private final SessionFactory sessionFactory;
    private final FilterHelper filterHelper;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory, FilterHelper filterHelper) {
        this.sessionFactory = sessionFactory;
        this.filterHelper = filterHelper;
    }

    @Override
    public void create(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policy);
        }
    }

    @Override
    public Policy update(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policy);
            session.getTransaction().commit();
            return policy;
        }
    }

    @Override
    public Policy getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Policy> criteriaQuery = criteriaBuilder.createQuery(Policy.class);
            Root<Policy> root = criteriaQuery.from(Policy.class);
            criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id));

            return session.createQuery(criteriaQuery).uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(String.format(POLICY_NOT_FOUND, id)));
        }
    }

    @Override
    public List<Policy> getAll() {
        try (Session session = sessionFactory.openSession()) {

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Policy> criteriaQuery = criteriaBuilder.createQuery(Policy.class);
            Root<Policy> root = criteriaQuery.from(Policy.class);
            criteriaQuery.select(root);

            return session.createQuery(criteriaQuery).list();
        }
    }

    @Override
    public List<Policy> filter(String user, Integer statusId, String sortBy, boolean isOrderAscending) {
        try (Session session = sessionFactory.openSession()) {

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Policy> criteriaQuery = criteriaBuilder.createQuery(Policy.class);
            Root<Policy> root = criteriaQuery.from(Policy.class);

            Predicate[] predicates = filterHelper.getPredicates(user, statusId, criteriaBuilder, root);

            filterHelper.sortQuery(sortBy, criteriaBuilder, criteriaQuery, root, isOrderAscending);

            criteriaQuery.select(root).where(criteriaBuilder.and(predicates));

            return session.createQuery(criteriaQuery).list();
        }
    }
}
