package com.safetyCar.repositories;

import com.safetyCar.models.impl.policy.Premium;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;

@Repository
public class CriteriaTableRepositoryImpl implements CriteriaTableRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CriteriaTableRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Premium getByCriteria(int cubicCapacity, int carAge) {
        try (Session session = sessionFactory.openSession()) {

            Query<Premium> baseAmount = session.createQuery(
                    "FROM Premium WHERE (:cubicCapacity BETWEEN ccMin AND ccMax) AND (:carAge BETWEEN carAgeMin AND carAgeMax)"
                    , Premium.class);
            baseAmount.setParameter("cubicCapacity", cubicCapacity);
            baseAmount.setParameter("carAge", carAge);

            return baseAmount
                    .stream()
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException("No base amount for these criteria found!"));
        }
    }

    @Override
    public Premium update(Premium premium) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(premium);
            session.getTransaction().commit();
            return premium;
        }
    }
}
