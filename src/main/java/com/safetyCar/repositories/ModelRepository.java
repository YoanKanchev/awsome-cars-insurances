package com.safetyCar.repositories;

import com.safetyCar.models.impl.car.Model;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<Model, Integer> {
}
