package com.safetyCar.repositories;

import com.safetyCar.helpers.FilterHelper;
import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;
    private final FilterHelper filterHelper;


    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, FilterHelper filterHelper) {
        this.sessionFactory = sessionFactory;
        this.filterHelper = filterHelper;
    }

    @Override
    public List<UserDetails> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> getAll = session.createQuery("FROM UserDetails", UserDetails.class);
            return getAll.list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> userQuery = session.createQuery("FROM UserDetails where id = :id", UserDetails.class);
            userQuery.setParameter("id", id);
            return userQuery
                    .stream()
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException(String.format("User with ID %s not found!", id)));
        }
    }

    @Override
    public UserDetails getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> userQuery = session
                    .createQuery(
                            "FROM UserDetails where userAuthentication.username like :username", UserDetails.class);
            userQuery.setParameter("username", username);
            return userQuery
                    .stream()
                    .findFirst()
                    .orElseThrow(() -> new EntityNotFoundException(String.format("User with username %s not found!", username)));
        }
    }

    @Override
    public UserAuthentication getUserAuth(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserAuthentication> userQuery = session
                    .createQuery(
                            "FROM UserAuthentication where username like :username", UserAuthentication.class);
            userQuery.setParameter("username", username);
            List<UserAuthentication> users = userQuery.list();
            return users.get(0);
        }
    }

    @Override
    public void createUser(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void updateUser(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            user.setUpdateDate(String.valueOf(LocalDate.now()));
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteUser(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public Optional<UserDetails> isUsernamePresent(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> userQuery = session.createQuery("FROM UserDetails where userAuthentication.username like :username", UserDetails.class);
            userQuery.setParameter("username", username);
            return userQuery.list()
                    .stream()
                    .findFirst();
        }
    }

    @Override
    public Optional<UserDetails> isEmailPresent(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> userQuery = session.createQuery("FROM UserDetails where email like :email", UserDetails.class);
            userQuery.setParameter("email", email);
            return userQuery.list().stream().findFirst();
        }
    }

    @Override
    public List<Policy> getPolicyByUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> policyQuery = session.createQuery("FROM Policy WHERE creator.id = :id", Policy.class);
            policyQuery.setParameter("id", id);
            return policyQuery.list().stream().sorted((a, b) -> b.getSubmittedDate().compareTo(a.getSubmittedDate()))
                    .collect(Collectors.toList());
        }
    }

    public List<UserDetails> filter(String username, String role) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<UserDetails> criteriaQuery = criteriaBuilder.createQuery(UserDetails.class);

            Root<UserDetails> root = criteriaQuery.from(UserDetails.class);

            Predicate[] predicates = filterHelper.getUserPredicates(username, role, criteriaBuilder, root);

            criteriaQuery.select(root).where(criteriaBuilder.and(predicates));

            return session.createQuery(criteriaQuery).list();
        }
    }
}

