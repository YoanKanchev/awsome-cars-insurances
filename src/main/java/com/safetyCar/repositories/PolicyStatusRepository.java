package com.safetyCar.repositories;

import com.safetyCar.models.impl.policy.PolicyStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PolicyStatusRepository extends JpaRepository<PolicyStatus, Integer> {
    PolicyStatus findByStatus(String name);
}
