package com.safetyCar.repositories;

import com.safetyCar.models.impl.policy.Policy;
import com.safetyCar.models.impl.policy.PolicyStatus;

import java.util.List;

public interface PolicyRepository  {

    void create(Policy policy);

    Policy update(Policy policy);

    Policy getById(int id);

    List<Policy> getAll();

    List<Policy> filter(String user, Integer statusId, String sortBy, boolean isOrderAscending);

}
