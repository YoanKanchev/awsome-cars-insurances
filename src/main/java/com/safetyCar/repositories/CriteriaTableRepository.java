package com.safetyCar.repositories;

import com.safetyCar.models.impl.policy.Premium;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriteriaTableRepository {

    Premium getByCriteria(int cubicCapacity, int carAge);

    Premium update(Premium premium);
}
