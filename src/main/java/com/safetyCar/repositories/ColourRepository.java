package com.safetyCar.repositories;

import com.safetyCar.models.impl.car.Colour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColourRepository extends JpaRepository<Colour, Integer> {
}
