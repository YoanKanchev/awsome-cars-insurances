package com.safetyCar.repositories;

import com.safetyCar.models.impl.UserAuthentication;
import com.safetyCar.models.impl.UserDetails;
import com.safetyCar.models.impl.policy.Policy;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<UserDetails> getAllUsers();

    UserDetails getById(int id);

    UserDetails getByUsername(String username);

    UserAuthentication getUserAuth(String username);

    void createUser(UserDetails user);

    void updateUser(UserDetails user);

    void deleteUser(UserDetails user);

    Optional<UserDetails> isEmailPresent(String email);

    Optional<UserDetails> isUsernamePresent(String username);

    List<Policy> getPolicyByUser(int id);

    List<UserDetails> filter(String username, String role);
}