# Awesome Cars Insurances

Yoan Kanchev and Yulian Stefanov group project


# Welcome to Awesome Cars Insurances
### Overview

Awesome Cars Insurances is an interactive web based app, that at its core is a app for calculating, sending requests and managing vehicle insurance policies.
A complex system of users is created, so depending on the user Admin, Agent, Regular, Guest, the app offers
different views and options...And much more.

### 1.The Database
The data of the application is stored on the **MariaDB** database server and can be easly recreated by the two files in the Database folder : 
- **scriptSafetyCar.sql** - responsible for creating the tables of the database;
- **scriptTestDataSafetyCar.sql** - inserting test data in the database.

The database is correctly normalised and data in the tables is not repeated. For the connection of the java source code and the databese we used **Hibernate ORM** as framework.

------------
##### Issues/Bugs/Ideas and further development

No known bugs for the time being

Ideas<br />
- to introduce other types of vehicles(motorcycles,transport vehicles ect.);
- to implement additional security verification for user log in - Google reCAPTCHA;


------------

### 2.The App Structure
#### Backend
First and foremost in the building of the app we wanted to follow as close as we can all the principles that we know for high quality code writing(**OOP, SOLID** ect.).
After that, with the help of **SpringMVC** and **SpringBoot** frameworks and using **Spring Security** to handle user registration and user roles, we built the structure in three layers:
- ##### Repository layer
This is the layer in which the connection between the java code, and the database is established by using **Hibernate**.
- ##### Service layer
In this layer we've represented the business logic of the app. Here you will find mostly validations for the policies and users while signing up along with role distribution and account changes.
- ##### Controller layer
This is the layer that makes the connection with the client, and it branches out to **Rest controllers** and **MVC controllers**.
The **Rest controllers** are connected with **Swagger** for improved testing quality.

#### Frontend
The frontend of **Awesome Cars Insurances** is written on **HTML** and **CSS** with the Html pages connecting via **MVC** controllers along with **Thymeleaf** template.
For our project to be modern and user-friendly, we complimented the **HTML** and **CSS** with the use of **JavaScript** and **JQuery** to achieve dynamic HTTP requests in the background in order to handle page reloading when a dynamic action need to take place.
As a layer of safety measures, we used validations for all the fields in the frontend part as well.

#### Tests
Tests are one of the most important parts of our app, as they help greatly to reduce the time for manual testing and keep track of implemented functionalities while developing new ones.
The service layer is covered with tests for 87% with the help of **JUnit** and **Mockito**

------------
##### Issues/Bugs/Ideas

No known bugs for the time being

Idea-to create tests for the repository and service layers.

------------
### 3.The Functionality

The app is split into four main parts, as these parts are depending on the user role - **GUEST, REGULAR, AGENT, ADMIN**.

#### GUEST
The GUEST is a user, that is not logged into the app system and has access to the following functionalities:

##### Homepage
The guest has access to the Homepage of the app, as he can see the info of the app in the contact section, and most importantly he can access the **Test Policy Calculator** - calculator, that is capable of outputting how much the insurance policy is about to cost, incl., all taxes, depending on the input data of the user (all fields are required).
The calculator is implemented with **JQuery** request, thus removing the need of page reloading.
##### LogIn/SignUP
The **LogIn/SignUP** menus are modals on the Homepage and gives the GUEST the ability to create his account and unlock the other functionalities of the app.
The SignUP has validations, that are visible, when you hover over each of the fields names.
Also it has automatic check (**JQuery**) if the **username** and **email** already exist in the sistem. 

After completing the registration every user will have verification e-mail send to their addres and their account will activate after visiting the provided URL and will become a **REGULAR** user.

------------
#### REGULAR
The **REGULAR** user is a user/client, that uses the application and has a registered profile and has verified his account by e-mail.
##### Homepage
The regular user has the same access to the **Test Policy Calculator** but now he will have unlocked more options, as he can request an **Insurance policy**, but first he must provide additional information for things like **Start date**, **Car Registration Certification** picture ect.(All the fields are requierd).
##### Profile 
All registered users have access to the **Profile** page, where they can update the details of their own profile.
##### My Insurances 
The regular user has dedicated page for all of his **requested insurances**, where he can update some of the information(e-mail, address, phone), and also withdraw the request if it is in **Pending** state.
#### AGENT
The **AGENT** user role in the app is intended for company front desk employees which main responsibilities are based on managing insurances requests.
<br/>
##### Agent portal
Agent portal tap is located in the navigation bar and is accessible only by user with role AGENT.

##### Insurances

The Insurances tap is located in the navigation bar after accessing the Agent portal.<br/><br/>
The page provides a table representation of all policies regardless of their status.
Upon accessing the page, the table is loaded with basic customer information on each row, along with the fields respective headers.
By using **CSS**, by hovering on each row, an additional fall down area is provided where more detail information of the respective request is presented.
<br/>
While policy being in **Pending** status, the agent has additional interaction with the respective request.
Provided in the form of customized buttons, he can either **Accept**, **Reject** or **Cancel** the policy, thus
updating the request status in the database and closing the request thus preventing any further updates from agent side.<br/><br/>
An additional functionality is triggered upon interacting with each button. By using java class - JavaMailSender,
an email is going to be constructed with pre-defined text and send to the customer with an update of the outcome for his request.<br/><br/>
If needed, the agent can interact with the table itself by customizing the data representation.<br/>
Implemented with custom **filter**, a agent has the functionality to search for policies by user **username or policy status**.
He can as well **sort** the results by **policy ID, Submitted date or Gross amount** in **ascending or descending order**.<br/><br/>
The filter and sort option can work independently of one another or by combinations among them.<br/><br/>
Upon status change, a respective notification is visualized in the page with the outcome status upon interaction.

------------
#### ADMIN
The **ADMIN** user role in the app is intended for company back desk employees which main responsibilities are based on managing the whole process.
<br/>
##### Admin portal
Admin portal tap is located in the navigation bar and is accessible only by user with role ADMIN.<br/>
It is used for navigation for the admins capabilities and as well as for **submitting update changes for the criteria table** from where base amount is calculated.<br/>
Currently a file upload is supported only with files with **CSV** extension and will show a respective feedback upon submission.
<br/>
##### Insurances
The Insurances tap is located in the navigation bar after accessing the Admin portal.<br/><br/>
The page has identical content and functionalities like the page in the **Agent portal**.<br/>
Additionally, the admin has the capability to revert the status of a policy. The functionality is presented in the form
of customized button and is possible to **Revert* a policy status only if the policy is not in pending status, and
the start date is not lesser than the today's date.<br/><br/>
A respective message will show the outcome of button interaction.
<br/>
##### Search user
The Search user tap is located in the navigation bar after accessing the Admin portal.<br/><br/>
The page provides a table representation of all users regardless of their role.
Upon accessing the page, the table is loaded with basic customer information on each row, along with the fields respective headers.
By using **CSS**, by hovering on each row, an additional fall down area is provided where more detail information of the respective user is presented.
<br/>
The admin has the capability to delete(soft) a user account regardless of their role. By doing so, the account is going to remain
in the database but the same would become inactive, thus preventing further access to the web application.
<br/>
The admin has the capability to provide admin rights to a user with agent role, thus providing further access to the application.
<br/>
Implemented with custom **filter**, a admin has the functionality to search for users by user **username or role**. 
<br/><br/>
An outcome message will appear after interaction with an update of the action.
<br/>
##### Create new user
The Create new user tap is located in the navigation bar after accessing the Admin portal.<br/><br/>
An admin is provided the capability of creating new technical users with the respective rights needed.
Similar to the sign up process, the newly created user has to match the pre-defined requirements of a user account.<br/>
- The new user has to have a unique username in the range between 4 and 20 symbols. The input is verified dynamically while typing and on click outside the box.
- The password must be in the range between 5 and 20 symbols and must match the pattern request for strong password:
<br/>
1. Must contain at least one lowercase character;
<br/>
2. Must contain at least one uppercase character;
<br/>
3. Must contain at least one digit character;
<br/>
4. Must contain at least one special character in the list -> @#$%;
- The first and last name must be in range between 2 and 20 symbols.
- The email has to be unique in the database and need to have @ and valid domain. An automatic check versus the database will be triggered once clicked outsite the box.
<br/><br/>
All fields are required in order to submit the new user! Appropriate message will be shown in case of failure.
<br/>
##### Statistics
The Statistics tap is located in the navigation bar after accessing the Admin portal.<br/><br/>
The page represents a basic navigation throughout the available charts set via a dropdown menu.
Currently, the supported charts variants are **Column, Line, Pie, Donut and Complex Line**.<br/>
The charts visualization is handled by implementing Google charts API and data is fed via Rest API,
where through JavaScript and google build in methods, provided by the respectful dependecy, the data
is being restructured and passed for visualization.<br/>
By using JavaScript, once the data is structured for one chart, there is no need for page reloading when
traversing throughout the filtering options provided.
